<?php

namespace App\Mail;

use App\DraftResponseGroup;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $group = DraftResponseGroup::where('in_use',1)->first();
        return $this->from("atomacu@gmail.com")
        ->subject($group->template($this->data->lang_name)->title)
        ->view('mail.autoresponse')
        ->with('text',$this->data);
    }
}
