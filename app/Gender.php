<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gender extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name'
    ];

    public function trans(){
        return $this->hasMany('App\GenderTrans',"gender_id");
    }

    public function genderTrans($langSysName) {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang = Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $lang = Langs::first();
            }
        }
        if($lang){
            return $this->hasMany('App\GenderTrans', 'gender_id')->where('lang_id',$lang->id)->first();
        }
    }
}
