<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactEmailType extends Model
{
    protected $fillable = [
        'name'
    ];

    public function trans($langSysName) {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang = Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $lang = Langs::first();
            }
        }
        if($lang){
            return $this->hasMany('App\EmailTypeTrans', 'contact_email_type_id')->where('lang_id',$lang->id)->first();
        }
    }
    
}
