<?php

namespace App;

use App\Calendar;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $with = ['calendar'];

    protected $fillable = [
        'project_id','session_id','calendar_id','google_id', 'name', 'description', 'allday', 'started_at', 'ended_at','status',
    ];

    public function calendar()
    {
        return $this->belongsTo(Calendar::class);
    }

    public function session()
    {
        return $this->belongsTo('App\Session',"session_id");
    }

    public function project()
    {
        return $this->belongsTo('App\Project',"project_id");
    }
}
