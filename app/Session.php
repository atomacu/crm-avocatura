<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    protected $fillable = [
        'form_id','date_time_start','date_time_end','price','status'
    ];

    public function consultants(){
        return $this->hasMany('App\SesionConsultant',"session_id");
    }

    public function mail(){
        return $this->belongsTo('App\ContactForm',"form_id");
    }

    public function event(){
        return $this->hasOne('App\Event',"session_id");
    }
}
