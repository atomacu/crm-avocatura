<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name', 'description', 'date_start', 'date_end', 'price', 'validation', 'payment',
    ];

    public function advisors(){
        return $this->hasMany('App\ProjectManager', "project_id");
    }

    public function projectRaports(){
        return $this->hasMany('App\ProjectRaport', "project_id");
    }

    public function event(){
        return $this->hasOne('App\Event',"project_id");
    }
    
    public function getPercentage(){
        $startDate = $this->date_start;
        $endDate = $this->date_end;
        $date1=date_create($startDate);
        $date2=date_create($endDate);
        $projectDays=date_diff($date1,$date2)->format("%R%a");
       
        $daysAgo = date_diff($date1, date_create(date("Y/m/d")))->format("%R%a");
        $percentage = (100*$daysAgo)/$projectDays;
        $finalPercentage = $percentage > 100 ? 100 : $percentage;
        return $finalPercentage;
    }




}
