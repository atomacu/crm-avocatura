<?php

namespace App;

use App\Langs;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DraftResponseGroup extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id','name','in_use',
    ];

    public function templates(){
        return $this->hasMany('App\DraftResponse',"draft_response_groups_id");
    }

    public function template($langName = ""){
        if($langName == ""){
            $lang=Langs::where('system_name',config('app.fallback_locale'))->first();
        }else{
            $lang=Langs::where('system_name',$langName)->first();
            
        }
        if(!$lang){
            $lang=Langs::first();
        }
        return $this->hasMany('App\DraftResponse',"draft_response_groups_id")->where('lang_id',$lang->id)->first();
    }
}
