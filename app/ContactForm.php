<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactForm extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name','email','phone','address','birth_date','gender_id','description','lang_name','status'
    ];


    public function response(){
        return $this->hasOne('App\ResponseContactForm',"form_id");
    }

    public function session(){
        return $this->hasOne('App\Session',"form_id");
    }
}
