<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenderTrans extends Model
{
    protected $fillable = [
        'name','gender_id','lang_id',
    ];
}
