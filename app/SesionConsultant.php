<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SesionConsultant extends Model
{
    protected $fillable = [
        "user_id", "session_id", "engagement_rate",
    ];

    public function user(){
        return $this->belongsTo('App\User',"user_id");
    }

    public function session(){
        return $this->belongsTo('App\Session',"session_id");
    }
}
