<?php

namespace App\Http\Middleware;

use App;
use Auth;
use App\Langs;
use Closure;

class Share
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()){
            App::setLocale(Auth::user()->default_lang);
        }
      
        // dump(App::getLocale());
     
        $langs=Langs::all();
        // dd($langs);
        view()->share('langs', $langs);
        view()->share('langSysName', App::getLocale());
        return $next($request);
    }
}
