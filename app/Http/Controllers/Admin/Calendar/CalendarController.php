<?php

namespace App\Http\Controllers\Admin\Calendar;

use App;
use Auth;
use Form;
use Calendar;
use Validator;
use App\Event;
use App\Langs;
use Carbon\Carbon;
use App\Services\Google;
use App\Concerns\Synchronizable;
use App\GoogleAccount;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $client;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($langSysName = "")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang=Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $langSysName = config('app.fallback_locale'); 
            }else{
                $langSysName = $lang->system_name;
            }
        }
        App::setLocale($langSysName);
        App\Jobs\PeriodicSynchronizations::dispatch();
        $events = auth()->user()->events()->get();
        $event_list = [];
        foreach ($events as $key => $event) {
            if($event->calendar_id==1){
                $allDay = $event->allday==1? true : false;
                $eventName = $event->status == "private"? "Privat info" : $event->name;

                if(isset($event->session_id)){
                    $color = "#b81b16";
                }elseif(isset($event->project_id)){
                    $color = "#fca101";
                }else{
                    $color = "#3691ff";
                }
               
                $event_list[] = Calendar::event(
                    $eventName,
                    $allDay,
                    new \DateTime($event->started_at),
                    new \DateTime($event->ended_at),
                    $event->id,
                    [
                        'color' => $color,
                        'description' => "Event Description",
                        'textColor' => '#0A0A0A'
                    ]
                );
            }
        }
      
        $calendar_details = Calendar::addEvents($event_list); 
        $accounts = auth()->user()->googleAccounts;
        return view('admin.calendar.index', compact('calendar_details','accounts') );
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Event::create([
            'event_name' => 'Eveniment',
            'start_date' => '2019-10-09',
            'end_date' => '2019-10-10'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(GoogleAccount $googleAccount, Google $google)
    {
        $googleAccount->calendars->each->delete();

        $googleAccount->delete();

        $google->revokeToken($googleAccount->token);

        return redirect()->back();
    }
}
