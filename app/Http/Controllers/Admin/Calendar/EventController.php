<?php

namespace App\Http\Controllers\Admin\Calendar;

use App\Event;
use App\Session;
use App\ContactForm;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id = "")
    {
        $event = Event::find($id);
      
        return $event;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id = "")
    {
        $started = date_format(date_create($request['dateTimeStart']) ,"Y-m-d H:i:s");
        $ended = date_format(date_create($request['dateTimeEnd']) ,"Y-m-d H:i:s");
        
        // dd($request->all());
       
        Event::where('id',$id)->update([
            'name' => $request['title'],
            'started_at' => $started,
            'ended_at' => $ended,
            'description' => $request['description'],
            'edit' => 1
        ]);
        $event = Event::find($id);
       
        Session::where('id', $event->session_id)->update([
            'date_time_start' => $started,
            'date_time_end' => $ended
        ]);

        return $event;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id = "")
    {
        
        Event::where("id",$id)->update([
            'delete' =>1
        ]);

        $event = Event::find($id);
      
        if(isset($event->session->mail->id)){
            ContactForm::where('id',$event->session->mail->id)->update([
                'status' => 0
            ]);
        }
       
        return $id; 
    }
}
