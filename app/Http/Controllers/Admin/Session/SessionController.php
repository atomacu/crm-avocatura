<?php

namespace App\Http\Controllers\Admin\Session;

use App;
use App\User;
use App\Event;
use App\Langs;
use App\Gender;
use App\Session;
use App\ContactForm;
use App\SesionConsultant;
use Illuminate\Http\Request;
use App\Services\Google;
use App\Http\Controllers\Controller;

class SessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($langSysName = "")
    { 
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang=Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $langSysName = config('app.fallback_locale'); 
            }else{
                $langSysName = $lang->system_name;
            }
        }
       
        App::setLocale($langSysName);
        $mails = ContactForm::where('status',2)->paginate(20);
        return view('admin.sessions.index',compact('mails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($langSysName = "")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang=Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $langSysName = config('app.fallback_locale'); 
            }else{
                $langSysName = $lang->system_name;
            }
        }
       
        App::setLocale($langSysName);
        $genders = Gender::get();
        $users = User::all();
      
        return view('admin.sessions.create',compact('genders','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $contact=ContactForm::create([
            'name' => $request['clientName'],
            'email' => $request['clientEmail'],
            'phone' => $request['clientPhone'],
            'address' => $request['clientAddress'],
            'birth_date' => $request['clientBirthDate'],
            'gender_id' => $request['clientGender'],
            'description' => $request['description'],
            'lang_name' => App::getLocale(),
            'status' => 2
        ]);

        Session::create([
            'form_id' => $contact->id,
            'date_time_start' => date_format(date_create($request['bookDateStart']) ,"Y-m-d H:i:s"),
            'date_time_end' => date_format( date_create($request['bookDateEnd']),"Y-m-d H:i:s"),
            'price' => $request['sessionPrice'],
        ]);
 
       
        if (empty($request['advisors'])) {
            SesionConsultant::where("session_id",$contact->session->id)->delete();
        }
        foreach($request['advisors'] as $advisor){
            $sessionConsultant = SesionConsultant::where('user_id', $advisor['advisorId'])->where("session_id",$contact->session->id)->first();

            if($sessionConsultant){
                SesionConsultant::where('user_id', $advisor['advisorId'])->where("session_id",$contact->session->id)->delete();
            }
           
            SesionConsultant::create([
                "user_id" => $advisor['advisorId'],
                "session_id" => $contact->session->id,
                "engagement_rate" => $advisor['advisorPercentage']
            ]);
    
            $user = User::find($advisor['advisorId']);
            if(count($user->googleAccounts)>0){
                $calendarId = $user->googleAccounts[0]->calendars[0]->id;
            }else{
                $calendarId=0;
            }
            
            Event::create([
                'calendar_id'=> $calendarId,
                'session_id' => $contact->session->id,
                'name'=>$request['clientName'],
                'description'=>$request['description'],
                'allday'=>0,
                'status' => "public",
                'started_at'=>date_format(date_create($request['bookDateStart']) ,"Y-m-d H:i:s"),
                'ended_at'=>date_format( date_create($request['bookDateEnd']),"Y-m-d H:i:s"),
            ]);
           

        }
        App\Jobs\PeriodicSynchronizations::dispatch();
        return route('admin-sessions.index');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = "", $langSysName = "")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang=Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $langSysName = config('app.fallback_locale'); 
            }else{
                $langSysName = $lang->system_name;
            }
        }
        App::setLocale($langSysName);
        $genders = Gender::get();
        $mail = ContactForm::find($id);
        $users = User::all();
        return view('admin.sessions.show', compact('mail','genders','users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
        ContactForm::where('id', $id)->update([
            'name' => $request['clientName'],
            'email' => $request['clientEmail'],
            'phone' => $request['clientPhone'],
            'address' => $request['clientAddress'],
            'birth_date' => $request['clientBirthDate'],
            'gender_id' => $request['clientGender'],
        ]);

        $contact = ContactForm::find($id);

        $contact->session->price = $request['sessionPrice'];
        $contact->session->save();

        if (empty($request['advisors'])) {
            SesionConsultant::where("session_id",$contact->session->id)->delete();
        }
        foreach($request['advisors'] as $advisor){
            $sessionConsultant = SesionConsultant::where('user_id', $advisor['advisorId'])->where("session_id",$contact->session->id)->first();

            if($sessionConsultant){
                SesionConsultant::where('user_id', $advisor['advisorId'])->where("session_id",$contact->session->id)->delete();
            }
           
            SesionConsultant::create([
                "user_id" => $advisor['advisorId'],
                "session_id" => $contact->session->id,
                "engagement_rate" => $advisor['advisorPercentage']
            ]);
        }
        
        return $id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function endSession($id){
        ContactForm::where('id',$id)->update([
            'status' => 3
        ]);
        return route('admin-sessions.index');
    }

    public function cancelSession($id){
        ContactForm::where('id',$id)->update([
            'status' => 0
        ]);
        $cForm = ContactForm::find($id);
        
        Event::where("id",$cForm->session->event->id)->update([
            'delete' =>1
        ]);
        return route('admin-sessions.index');
    }
    // {"scope": "https://www.googleapis.com/auth/calendar https://www.googleapis.com/auth/userinfo.email openid", "created": 1571237934, "id_token": "eyJhbGciOiJSUzI1NiIsImtpZCI6IjNkYjNlZDZiOTU3NGVlM2ZjZDlmMTQ5ZTU5ZmYwZWVmNGY5MzIxNTMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL2FjY291bnRzLmdvb2dsZS5jb20iLCJhenAiOiI3MDEzNDM1Njc1MDAtcmI0bmc0bWpoYzVnamthNWt1NmJqNzd0MzEwaWJka2cuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI3MDEzNDM1Njc1MDAtcmI0bmc0bWpoYzVnamthNWt1NmJqNzd0MzEwaWJka2cuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTQzOTY4NDgyMjg1MTM2MzEwODkiLCJlbWFpbCI6ImF0b21hY3VAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsImF0X2hhc2giOiJhaVV5eDkzSGp3dVZ3ZVZyT0d1UU5RIiwiaWF0IjoxNTcxMjM3OTM0LCJleHAiOjE1NzEyNDE1MzR9.zREVJuJ9bZkxs_EQ0FeE_t3FpNKQrn32N415EM7XRBHP0mL3QG2BcF2YiGSiASrmwiwPiEZTf6UFU6_6wNKKD941AYFOC_k5fu9sbBu0o8YM1GwezFdlhY-ehnAXCUh6dDOOVYInOMDM_e5W_M-_LZVnjjxLgn7wawHJy0lFuUaf90qjbBeQxdPkZLyKXE98M6uwLREWARNL9G7wCxdEQmpFGQZ8tnDvbcZ8lhq3ORlbn9HJnCFoO5EhdqluDf25t3zgcbs-XiLZPv-YO6PQfz9LcHnqsDBWvSA27db4U5Q9Q5LbbZ3HMxD3hzFzwIZhRX9iHR3GoPggISpc2ehKMg", "expires_in": 3600, "token_type": "Bearer", "access_token": "ya29.Il-bB3-KKPSRkxbOwxDciddbFWX_ct2AjhOTSQPTyff489hea0uRekuUXUstIenIW9EuXtYi4ElUNycFrJRTQIQVYjI_AQC_ph0-Quq8omYyzDX0puPk51UxaO1QyqKdUQ", "refresh_token": "1//03tNXA8CNPaxdCgYIARAAGAMSNwF-L9Ir73Vxsv62XgfB3xU2DpFRunRc0ID08dNkFoVEMp0YiLBD5XWYrbJpEtpc6Evaxu9ntj4"}

    public function CreateCalendarEvent($calendar_id, $summary, $all_day, $event_time, $event_timezone, $access_token) {
		$url_events = 'https://www.googleapis.com/calendar/v3/calendars/' . $calendar_id . '/events';

		$curlPost = array('summary' => $summary);
		if($all_day == 1) {
			$curlPost['start'] = array('date' => $event_time['event_date']);
			$curlPost['end'] = array('date' => $event_time['event_date']);
		}
		else {
			$curlPost['start'] = array('dateTime' => $event_time['start_time'], 'timeZone' => $event_timezone);
			$curlPost['end'] = array('dateTime' => $event_time['end_time'], 'timeZone' => $event_timezone);
		}
		$ch = curl_init();		
		curl_setopt($ch, CURLOPT_URL, $url_events);		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);		
		curl_setopt($ch, CURLOPT_POST, 1);		
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. $access_token['access_token'], 'Content-Type: application/json'));	
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($curlPost));	
		$data = json_decode(curl_exec($ch), true);
        $http_code = curl_getinfo($ch,CURLINFO_HTTP_CODE);	
        dump($http_code);	
		if($http_code != 200) 
			throw new Exception('Error : Failed to create event');

		return $data['id'];
	}

}
