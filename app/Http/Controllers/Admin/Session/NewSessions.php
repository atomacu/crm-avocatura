<?php

namespace App\Http\Controllers\Admin\Session;

use App;
use App\User;
use App\Langs;
use App\Gender;
use App\ContactForm;
use App\SesionConsultant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewSessions extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($langSysName = "")
    { 
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang=Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $langSysName = config('app.fallback_locale'); 
            }else{
                $langSysName = $lang->system_name;
            }
        }
        App::setLocale($langSysName);
        $mails = ContactForm::where('status',2)->paginate(20);

        return view('admin.new-sessions.index',compact('mails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = "", $langSysName = "")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang=Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $langSysName = config('app.fallback_locale'); 
            }else{
                $langSysName = $lang->system_name;
            }
        }
        App::setLocale($langSysName);
        $genders = Gender::get();
        
        $mail = ContactForm::find($id);

        $users = User::all();


        return view('admin.new-sessions.show', compact('mail','genders','users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
        ContactForm::where('id', $id)->update([
            'name' => $request['clientName'],
            'email' => $request['clientEmail'],
            'phone' => $request['clientPhone'],
            'address' => $request['clientAddress'],
            'birth_date' => $request['clientBirthDate'],
            'gender_id' => $request['clientGender'],
        ]);

        $contact = ContactForm::find($id);

        $contact->session->price = $request['sessionPrice'];
        $contact->session->save();

        if (empty($request['advisors'])) {
            SesionConsultant::where("session_id",$contact->session->id)->delete();
        }
        foreach($request['advisors'] as $advisor){
            $sessionConsultant = SesionConsultant::where('user_id', $advisor['advisorId'])->where("session_id",$contact->session->id)->first();

            if($sessionConsultant){
                SesionConsultant::where('user_id', $advisor['advisorId'])->where("session_id",$contact->session->id)->delete();
            }
           
            SesionConsultant::create([
                "user_id" => $advisor['advisorId'],
                "session_id" => $contact->session->id,
                "engagement_rate" => $advisor['advisorPercentage']
            ]);
        }
        
        return $id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function newAdvisor()
    {
        $users = User::all();
        $html = view('admin.new-sessions.components.session-consultant',compact('users'))->render();
        return json_encode($html);
    }

    public function getUserGoogle($id){
        $user = User::find($id);
        if(count($user->googleAccounts)>0){
            $token = $user->googleAccounts[0]->token;
            $calendarId = $user->googleAccounts[0]->calendars[0]->id;
        }

        return [$calendarId,$token];
    }

    public function cancelNewSession($id){
        ContactForm::where('id',$id)->update([
            'status' => 0
        ]);
        return route('admin-new-sessions.index');
    }
}
