<?php

namespace App\Http\Controllers\Admin\Project;

use App;
use Auth;
use App\User;
use App\Event;
use App\Langs;
use App\Project;
use App\ProjectRaport;
use App\ProjectManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($langSysName = "")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang=Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $langSysName = config('app.fallback_locale'); 
            }else{
                $langSysName = $lang->system_name;
            }
        }
        App::setLocale($langSysName);

        $projects = Project::where('validation', 1)->paginate(20);
        return view('admin.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $project=Project::create([
            'name' => $request['name'],
            'description' => $request['description'],
            'date_start' => $request['dateStart'],
            'date_end' => $request['dateEnd'],
            'price' => $request['price'],
            'validation' => 1,
            'payment' => 0,
        ]);
       
        if (empty($request['advisors'])) {
            ProjectManager::where("project_id", $project->id)->delete();
        }
        foreach($request['advisors'] as $advisor){
            $projectManager = ProjectManager::where('user_id', $advisor['advisorId'])->where("project_id", $project->id)->first();

            if($projectManager){
                ProjectManager::where('user_id', $advisor['advisorId'])->where("project_id", $project->id)->delete();
            }
           
            ProjectManager::create([
                "user_id" => $advisor['advisorId'],
                "project_id" => $project->id,
                "engagement_rate" => $advisor['advisorPercentage']
            ]);

            $user = User::find($advisor['advisorId']);
            if(count($user->googleAccounts)>0){
                $calendarId = $user->googleAccounts[0]->calendars[0]->id;
            }else{
                $calendarId=0;
            }

            Event::create([
                'calendar_id'=> $calendarId,
                'project_id' => $project->id,
                'name'=>$request['name'],
                'description'=>$request['description'],
                'allday'=>1,
                'status' => "public",
                'started_at'=>date_format(date_create($request['dateStart']) ,"Y-m-d H:i:s"),
                'ended_at'=>date_format(date_add(date_create($request['dateEnd']), date_interval_create_from_date_string('12 hours')) ,"Y-m-d H:i:s"),
            ]);
            
        }
        App\Jobs\PeriodicSynchronizations::dispatch();
        return $project->id;
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = "", $langSysName = "")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang=Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $langSysName = config('app.fallback_locale'); 
            }else{
                $langSysName = $lang->system_name;
            }
        }
        App::setLocale($langSysName);

        $project = Project::find($id);
        return view('admin.projects.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $project = Project::find($id);
        // dd($project->event);
        $project = Project::find($id);
        Event::where("id",$project->event['id'])->update([
            'delete' =>1
        ]);
        Project::where('id',$id)->delete();
        return $id;
    }

    public function addRaport(Request $request){
       
        return ProjectRaport::create([
            'user_id' => Auth::user()->id,
            'project_id' => $request['id'],
            'description' => $request['description']
        ]);
    }

    public function newAdvisor()
    {
        $users = User::all();
        $html = view('admin.projects.components.session-consultant',compact('users'))->render();
        return json_encode($html);
    }
}
