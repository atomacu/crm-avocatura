<?php

namespace App\Http\Controllers\Admin\ContactForm;

use App;
use Auth;
use App\Langs;
use App\ContactForm;
use App\ResponseContactForm;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail2;
use App\Http\Controllers\Controller;

class ContactFormController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($langSysName = "")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang=Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $langSysName = config('app.fallback_locale'); 
            }else{
                $langSysName = $lang->system_name;
            }
        }
        $mails = ContactForm::orderBy('updated_at','desc')->where('status', 1)->paginate(12);
        return view('admin.mails.send.index', compact('mails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response = ResponseContactForm::create([
            'user_id' => Auth::user()->id,
            'form_id' => $request['id'],
            'title' => $request['subject'],
            'description' => $request['text'],
        ]);

        $contact = ContactForm::find($request['id']);
        Mail::to($contact->email)->send(new SendMail2($response));

        ContactForm::where('id',$request['id'])->update([
            'status' => 1
        ]);

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, $langSysName = "")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang=Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $langSysName = config('app.fallback_locale'); 
            }else{
                $langSysName = $lang->system_name;
            }
        }
        App::setLocale($langSysName);

        $email = ContactForm::find($id);
        return view('admin.mails.show', compact('email'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ContactForm::find($id)->delete();
        return $id;
    }
}
