<?php

namespace App\Http\Controllers\Admin\Mail;

use App;
use Auth;
use App\Langs;
use App\DraftResponse;
use App\DraftResponseGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GroupTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($langSysName = ""){
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang=Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $langSysName = config('app.fallback_locale'); 
            }else{
                $langSysName = $lang->system_name;
            }
        }
        App::setLocale($langSysName);

        $emailsTemplateGroup = DraftResponseGroup::orderBy('id','desc')->paginate(12);
        return view('admin.mails.template-email-group.index', compact('emailsTemplateGroup'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $emailTemplateGroup = DraftResponseGroup::create([
            'user_id' => Auth::user()->id,
            'name' => $request['name']
        ]);
      
        foreach($request['emails'] as $email){
          
            DraftResponse::create([
                'draft_response_groups_id' => $emailTemplateGroup->id,
                'lang_id' => $email['lang'],
                'title' => $email['subject'],
                'description' => $email['text'],  
            ]);
        }

        $html = view('admin.mails.components.email-group',compact('emailTemplateGroup'))->render();
        return json_encode($html);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   $emailTemplateGroup = DraftResponseGroup::find($id);
        return view('admin.mails.template-email-group.show',compact('emailTemplateGroup'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DraftResponseGroup::where('id',$id)->update([
            'name' => $request['name']
        ]);

        return $id;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DraftResponseGroup::find($id)->delete();
        return $id;
    }

    public function updateInUse(Request $request, $id)
    {
        DraftResponseGroup::where('in_use',1)->update([
            'in_use' => 0
        ]);

        DraftResponseGroup::where('id',$id)->update([
            'in_use' => 1
        ]);

        return $id;

    }
}
