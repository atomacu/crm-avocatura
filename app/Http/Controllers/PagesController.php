<?php

namespace App\Http\Controllers;

use App;
use App\Langs;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function wellcome($langSysName="") {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang=Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $langSysName = config('app.fallback_locale'); 
            }else{
                $langSysName = $lang->system_name;
            }
        }
        App::setLocale($langSysName);
        return view('welcome',compact('langSysName'));
    }
}
