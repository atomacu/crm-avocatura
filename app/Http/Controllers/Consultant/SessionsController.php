<?php

namespace App\Http\Controllers\Consultant;

use App;
use App\User;
use App\Langs;
use App\Gender;
use App\Session;
use App\ContactForm;
use App\SesionConsultant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SessionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($langSysName = "")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang=Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $langSysName = config('app.fallback_locale'); 
            }else{
                $langSysName = $lang->system_name;
            }
        }
        App::setLocale($langSysName);
        $genders = Gender::get();
        $users = User::all();
        return view('consultant.sessions.create',compact('genders','users'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contact=ContactForm::create([
            'name' => $request['clientName'],
            'email' => $request['clientEmail'],
            'phone' => $request['clientPhone'],
            'address' => $request['clientAddress'],
            'birth_date' => $request['clientBirthDate'],
            'gender_id' => $request['clientGender'],
            'description' => $request['description'],
            'lang_name' => App::getLocale(),
            'status' => 2
        ]);

        Session::create([
            'form_id' => $contact->id,
            'date' => $request['bookDate'],
            'time_start' => $request['bookStartTime'],
            'time_end' => $request['bookEndTime'],
            'price' => $request['sessionPrice'],
        ]);

       
        if (empty($request['advisors'])) {
            SesionConsultant::where("session_id",$contact->session->id)->delete();
        }
        foreach($request['advisors'] as $advisor){
            $sessionConsultant = SesionConsultant::where('user_id', $advisor['advisorId'])->where("session_id",$contact->session->id)->first();

            if($sessionConsultant){
                SesionConsultant::where('user_id', $advisor['advisorId'])->where("session_id",$contact->session->id)->delete();
            }
           
            SesionConsultant::create([
                "user_id" => $advisor['advisorId'],
                "session_id" => $contact->session->id,
                "engagement_rate" => $advisor['advisorPercentage']
            ]);
        }
        
        return route('consultant.index');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id = "", $langSysName = "")
    {
        if($langSysName == ""){
            $langSysName = config('app.fallback_locale'); 
        }else{
            $lang=Langs::where('system_name',$langSysName)->first();
            if(!$lang){
                $langSysName = config('app.fallback_locale'); 
            }else{
                $langSysName = $lang->system_name;
            }
        }
        App::setLocale($langSysName);
        $genders = Gender::get();
        $mail = ContactForm::find($id);
        $users = User::all();
        return view('consultant.sessions.show', compact('mail','genders','users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function endSession($id){
        ContactForm::where('id',$id)->update([
            'status' => 3
        ]);
        return route('consultant.index');
    }

    public function cancelSession($id){
        ContactForm::where('id',$id)->update([
            'status' => 0
        ]);
        return route('consultant.index');
    }
}
