<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectManager extends Model
{
    protected $fillable = [
        'user_id', 'project_id', 'engagement_rate',
    ];

    public function user(){
        return $this->belongsTo('App\User',"user_id");
    }

}
