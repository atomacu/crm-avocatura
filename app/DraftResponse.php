<?php

namespace App;

use App\Langs;
use Illuminate\Database\Eloquent\Model;

class DraftResponse extends Model
{
    protected $fillable = [
        'draft_response_groups_id','lang_id','title','description',
    ];

    public function lang(){
        return $this->belongsTo('App\Langs',"lang_id");
    }

}
