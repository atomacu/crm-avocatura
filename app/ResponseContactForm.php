<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResponseContactForm extends Model
{
    protected $fillable = [
        'user_id','form_id','title','description'
    ];
}
