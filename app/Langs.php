<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Langs extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'name','system_name'
    ];

    static function getLangBySysName($sysName){
        return parent::where('system_name',$sysName)->first();
    }
}
