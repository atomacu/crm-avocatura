<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectRaport extends Model
{
    protected $fillable = [
        'user_id', 'project_id', 'description',
    ];

    public function project(){
        return $this->belongsTo('App\Project',"project_id");
    }

    public function user(){
        return $this->belongsTo('App\User',"user_id");
    }

    public function getPercentage(){
        $startDate = $this->project->date_start;
        $endDate = $this->project->date_end;
        $date1=date_create($startDate);
        $date2=date_create($endDate);
        $projectDays=date_diff($date1,$date2)->format("%R%a");
        $daysAgo = date_diff($date1,date_create($this->created_at))->format("%R%a");
        $percentage = (100*$daysAgo)/$projectDays;
        $finalPercentage = $percentage > 100 ? 100 : $percentage;
        return $finalPercentage;
    }
}
