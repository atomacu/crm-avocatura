<?php

namespace App\Jobs;
USE DB;
use DateTime;
use DateTimeZone;
use App\Event;
use App\GoogleAccount;

abstract class SynchronizeGoogleResource
{
    protected $synchronizable;
    protected $synchronization;

    public function __construct($synchronizable)
    {
        $this->synchronizable = $synchronizable;
        $this->synchronization = $synchronizable->synchronization;
    }

    public function handle()
    {
        $pageToken = null;
        $syncToken = $this->synchronization->token;
        $service = $this->synchronizable->getGoogleService('Calendar');
       
        $events = Event::where('google_id',null)->get();
       
        $eventsDeleted = Event::where('delete',1)->get();
        $eventsEdit = Event::where('edit',1)->get();

        foreach($events as $localevent){

            $date=date_create($localevent->started_at);
            $objDateTime = new DateTime(date_format($date,"Y-m-d H:i:s"));
         
            
            $isoDateStart = $objDateTime->format(DateTime::ISO8601);
           
            $objDateTime = new DateTime($localevent->ended_at);
           
            $isoDateEnd = $objDateTime->format(DateTime::ISO8601);

           
            $event = new \Google_Service_Calendar_Event(array(
                'summary' => $localevent->name,
                'description' => $localevent->description,
                'start' => array(
                    'dateTime' => $isoDateStart,
                    'timeZone' => $localevent->calendar->timezone,
                ),
                'end' => array(
                    'dateTime' => $isoDateEnd,
                    'timeZone' => $localevent->calendar->timezone,
                ),
            ));
            
              $calendarId = 'primary';
              $event = $service->events->insert($calendarId, $event);

              
              Event::where('id',$localevent->id)->update([
                  'google_id' => $event->id
              ]);
            
             
        }
        foreach($eventsEdit as $event1){
            $event = $service->events->get('primary', $event1->google_id);
         
            $objDateTime = new DateTime($event1->started_at);
            
            $isoDateStart = $objDateTime->format(DateTime::ISO8601);

            $objDateTime = new DateTime($event1->ended_at);
            
            $isoDateEnd = $objDateTime->format(DateTime::ISO8601);

            $event->setSummary($event1->name);
            $event->setDescription($event1->description);
            $isoDateStartService = new \Google_Service_Calendar_EventDateTime();
            $isoDateStartService->setDateTime($isoDateStart);
            $isoDateEndService = new \Google_Service_Calendar_EventDateTime();
            $isoDateEndService->setDateTime($isoDateEnd);
            $event->setStart($isoDateStartService);
            $event->setEnd($isoDateEndService);
            
            $updatedEvent = $service->events->update('primary', $event->getId(), $event);
            $updatedEvent->getUpdated();
            $event1->edit = 0;
            $event1->save();
        }


        foreach($eventsDeleted as $event){
            $eventDomain=  $event->calendar_id==1 ? 'primary': "global";
            $service->events->delete('primary', $event->google_id);
            $event->delete = 0;
            $event->save();
        }

       
        
        do {
            $tokens = compact('pageToken', 'syncToken');

            try {
                $list = $this->getGoogleRequest($service, $tokens); 
            } catch (\Google_Service_Exception $e) {    
                if ($e->getCode() === 410) {
                    $this->synchronization->update(['token' => null]);  
                    $this->dropAllSyncedItems();    
                    return $this->handle(); 
                }   
                throw $e;   
            }

            foreach ($list->getItems() as $item) {
                $this->syncItem($item);
            }
            $pageToken = $list->getNextPageToken();
        } while ($pageToken);
        
        $this->synchronization->update([
            'token' => $list->getNextSyncToken(),
            'last_synchronized_at' => now(),
        ]);
    }

    abstract public function getGoogleRequest($service, $options);
    abstract public function syncItem($item);
    abstract public function dropAllSyncedItems();
}
