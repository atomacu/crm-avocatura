<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailTypeTrans extends Model
{
    protected $fillable = [
        "name", "contact_email_type_id", "lang_id",
    ];

}
