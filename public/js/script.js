class MyUploadAdapter {
    constructor(loader) {
        this.loader = loader;
    }
    upload() {
        return this.loader.file
            .then(file => new Promise((resolve, reject) => {
                this._initRequest();
                this._initListeners(resolve, reject, file);
                this._sendRequest(file);
            }));
    }
    abort() {
        if (this.xhr) {
            this.xhr.abort();
        }
    }
    _initRequest() {
        const xhr = this.xhr = new XMLHttpRequest();
        xhr.open('POST', '/image/upload', true);
        xhr.responseType = 'json';
    }
    _initListeners(resolve, reject, file) {
        const xhr = this.xhr;
        const loader = this.loader;
        const genericErrorText = `Couldn't upload file: ${ file.name }.`;

        xhr.addEventListener('error', () => reject(genericErrorText));
        xhr.addEventListener('abort', () => reject());
        xhr.addEventListener('load', () => {
            const response = xhr.response;
            if (!response || response.error) {
                return reject(response && response.error ? response.error.message : genericErrorText);
            }
            resolve({
                default: response.url
            });
        });
        if (xhr.upload) {
            xhr.upload.addEventListener('progress', evt => {
                if (evt.lengthComputable) {
                    loader.uploadTotal = evt.total;
                    loader.uploaded = evt.loaded;
                }
            });
        }
    }
    _sendRequest(file) {
        const data = new FormData();

        data.append('upload', file);
        this.xhr.send(data);
    }
}

function MyCustomUploadAdapterPlugin(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = (loader) => {
        return new MyUploadAdapter(loader);
    };
}


function formatDate(date) {
    console.log(date);
    var hours = date.getHours();
    console.log(hours);
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;

    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + " " + strTime;
}

$(document).ready(function () {

    $('.editor').each(function () {
        let name = '#editor-' + $(this).data('lang-id');
        ClassicEditor
            .create(document.querySelector(name), {
                extraPlugins: [MyCustomUploadAdapterPlugin],
            })
            .then(editor => {
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
    });

    $('#wellcome-contact-form').submit(function (event) {
        event.preventDefault();
        let url = $(this).data('url');
        let lang = $(this).data('lang-name');
        let name = $("#name").val();
        let email = $("#email").val();
        let phone = $("#phone").val();
        let description = $("#description").val();
        $('.loader').each(function () {
            $(this).css("display", "block");
        });

        let json = {
            "lang": lang,
            "name": name,
            "email": email,
            "phone": phone,
            "description": description
        };

        axios.post(url, json).then(function (response) {
            $('.loader').each(function () {
                $(this).css("display", "none");
            });
            $("#confirm-send-alert").trigger('click');
            setTimeout(function () {
                location.reload();
            }, 1500);
        }).catch(function (error) {
            console.log(error);
        });
    });

    $("#delete-emails-button").click(function () {
        let emailsId = [];

        $('.email-check').each(function () {
            if ($(this).is(':checked')) {
                emailsId.push($(this).data('delete-url'));
            }
        });
        emailsId.forEach(function (url) {
            axios.delete(url).then(function (response) {
                location.reload();
            }).catch(function (error) {
                console.log(error);
            });
        });
    });

    $('#add-response-template-group').submit(function (e) {
        e.preventDefault();
        let url = $(this).data('url');
        let name = $('#template-group-name').val();

        let emails = [];
        $('.subject-tmplate-group').each(function () {
            let langId = $(this).data('lang-id')
            let json = {
                lang: langId,
                subject: $(this).val(),
                text: $("#editor-" + langId).val()
            };
            emails.push(json);
        });

        let json = {
            name: name,
            emails: emails
        };

        axios.post(url, json).then(function (response) {
            $('[data-dismiss="modal"]').trigger('click');
            $('#email-template-group-list').append(response.data);
        }).catch(function (error) {
            console.log(error);
        });
    });

    $(document).on('click', '.delete-email-template-group', function (e) {
        let id = $(this).data('id');
        let url = $(this).data('url');

        axios.delete(url).then(function (response) {
            $('[data-dismiss="modal"]').trigger('click');
            setTimeout(function () {
                $('#' + id).remove();
            }, 350);
        }).catch(function (error) {
            console.log(error);
        });
    });

    $(document).on('submit', '.edit-template-group-name-form', function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        let url = $(this).data('url');
        let name = $('#edit-template-group-name-' + id).val();

        let json = {
            name: name
        };
        axios.put(url, json).then(function (response) {
            $('[data-dismiss="modal"]').trigger('click');
            $('#group-template-name-' + id).text(name);
        }).catch(function (error) {
            console.log(error);
        });
    });

    $(document).on('change', '.set-in-use-template', function (e) {
        let url = $(this).data('url');
        $('.set-in-use-template').each(function () {
            $(this).prop("checked", false);
        });
        $(this).prop("checked", true);

        axios.put(url).then(function (response) {

        }).catch(function (error) {
            console.log(error);
        });
    });

    $('.email-template-edit-form').submit(function (e) {
        e.preventDefault();
        let url = $(this).data('url');
        let langId = $(this).data('lang-id');
        let subject = $('#email-template-edit-' + langId).val();
        let text = $("#editor-" + langId).val();

        let json = {
            subject: subject,
            text: text
        };

        axios.put(url, json).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });

    $('#email-check-all').click(function () {
        if ($(this).prop('checked') != false) {
            $('.email-check').each(function () {
                $(this).prop('checked', true);
            })
        } else {
            $('.email-check').each(function () {
                $(this).prop('checked', false);
            })
        }

    });

    $('.a-redirect').click(function () {
        window.location.href = $(this).parent().data('redirect-url');
    });

    $('#response-contact-form-form').submit(function (e) {
        e.preventDefault();
        let id = $(this).data('email-id');
        let url = $(this).data('url');
        let subject = $('#response-contact-form-subject').val();
        let text = $('#editor-1').val();
        $('.loader').each(function () {
            $(this).css("display", "block");
        });
        let json = {
            id: id,
            subject: subject,
            text: text
        };

        axios.post(url, json).then(function (response) {
            window.location.replace(document.referrer);
        }).catch(function (error) {
            console.log(error);
        });

    });

    $('#booking-form').submit(function (e) {
        e.preventDefault();
        let id = $(this).data('form-id');
        let url = $(this).data('url');
        let date = $('#book-session-date').val();
        let start = $('#book-session-start-time').val();
        let end = $('#book-session-end-time').val();

        let json = {
            id: id,
            date: date,
            start: start,
            end: end
        };

        axios.post(url, json).then(function (response) {
            window.location.replace(response.data);
        }).catch(function (error) {
            console.log(error);
        });
    });

    $('#add-gender-type-form').submit(function (e) {
        e.preventDefault();
        let url = $(this).data('url');
        let generalName = $('#add-gender-type-general-name').val();
        let names = [];
        $('.gender-name-trans').each(function () {
            let json = {
                langId: $(this).data('lang-id'),
                name: $(this).val()
            };
            names.push(json);
        });

        let json = {
            generalName: generalName,
            names: names
        };

        axios.post(url, json).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });

    $(document).on('click', '#enrole-consultant-session', function (e) {
        let url = $(this).data('url');
        $('.error').remove();
        axios.get(url).then(function (response) {
            $('#advisor-list').append(response.data);
        }).catch(function (error) {
            console.log(error);
        });
    });

    $(document).on('click', '#delete-advisor', function (e) {
        $(this).parent().parent().remove();
    });

    $(document).on('submit', '#session-confirm-form', function (e) {
        e.preventDefault();
        let url = $(this).data('url');
        let clientName = $('#session-client-infor-name').val();
        let clientEmail = $('#session-client-infor-email').val();
        let clientPhone = $('#session-client-infor-phone').val();
        let clientAddress = $('#session-client-infor-address').val();
        let clientBirthDate = $('#session-client-infor-birth_date').val();
        let clientGender = $('#session-client-infor-gender option:selected').val();
        let sessionPrice = $('#session-price').val();
        let advisors = [];
        let advisorsIDs = [];
        let advisorsIdIncludes = false;
        let advisorsCount = 0;
        let advisorComplete = true;
        let check = 0;
        $('.error').remove();
        let advisorTotalPercentage = 0;

        $('.advisor-engagement-percentage').each(function () {
            advisorsCount++;
            let advisorId = $(this).parent().parent().parent().children('div.col-md-5').children('select.advisor-engagement-select').children('option:selected').val();
            let advisorPercentage = $(this).val();

            let json = {
                advisorId: advisorId,
                advisorPercentage: advisorPercentage
            };
            if (advisorId != "" && advisorPercentage != "") {
                advisorTotalPercentage += parseInt(advisorPercentage);
                advisors.push(json);
                if (!advisorsIdIncludes) {
                    advisorsIdIncludes = advisorsIDs.includes(advisorId)
                }
                advisorsIDs.push(advisorId);
            } else {
                advisorComplete = false;
                advisorsIdIncludes = false;
            }
        });


        if (!advisorComplete) {
            $("#advisor-list").append('<p class="error text-danger text-center">Information about advisor are not full or some fields are empty.<p>');
        }




        if (advisorsIdIncludes) {
            $("#advisor-list").append('<p class="error text-danger text-center">You use twice same advisor for this session.<p>')
        } else if (!advisorsIdIncludes || advisorsCount == 0) {
            check++;
        }

        if (advisorTotalPercentage == 100 || advisorsCount == 0) {
            check++;
        } else if ((advisorTotalPercentage < 100 || advisorTotalPercentage > 100) && advisorComplete) {
            $("#advisor-list").append('<p class="error text-danger text-center">Advisor percentage are set wrong.<p>');
        }

        let json = {
            clientName: clientName,
            clientEmail: clientEmail,
            clientPhone: clientPhone,
            clientAddress: clientAddress,
            clientBirthDate: clientBirthDate,
            clientGender: clientGender,
            sessionPrice: sessionPrice,
            advisors: advisors,
        };

        if (check == 2) {
            axios.put(url, json).then(function (response) {}).catch(function (error) {
                console.log(error);
            });
        }

    });

    $('#finish-session-confirm-button').click(function () {
        let url = $(this).data('url');
        axios.put(url).then(function (response) {
            window.location.href = response.data;
        }).catch(function (error) {
            console.log(error);
        });
    });

    $('#cancel-session-confirm-button').click(function () {
        let url = $(this).data('url');
        axios.put(url).then(function (response) {
            window.location.href = response.data;
        }).catch(function (error) {
            console.log(error);
        });
    });

    $('#cancel-new-session-confirm-button').click(function () {
        let url = $(this).data('url');
        axios.put(url).then(function (response) {
            window.location.href = response.data;
        }).catch(function (error) {
            console.log(error);
        });
    });



    $(document).on('submit', '#admin-session-add-form', function (e) {
        e.preventDefault();
        let url = $(this).data('url');

        let clientName = $('#admin-session-add-client-infor-name').val();
        let clientEmail = $('#admin-session-add-client-infor-email').val();
        let clientPhone = $('#admin-session-add-client-infor-phone').val();
        let clientAddress = $('#admin-session-add-client-infor-address').val();
        let clientBirthDate = $('#admin-session-add-client-infor-birth_date').val();
        let clientGender = $('#admin-session-add-client-infor-gender option:selected').val();
        let description = $("#editor-1").val();
        let sessionPrice = $('#admin-session-add-price').val();

        let bookDateStart = $("#admin-session-add-book-start-date-time-input").val();
        let bookDateEnd = $("#admin-session-add-book-end-date-time-input").val();


        // console.log(bookDateStart, bookDateEnd);


        let advisors = [];
        let advisorsIDs = [];
        let advisorsIdIncludes = false;
        let advisorsCount = 0;
        let advisorComplete = true;
        let check = 0;
        $('.error').remove();
        let advisorTotalPercentage = 0;


        $('.advisor-engagement-percentage').each(function () {
            advisorsCount++;
            let advisorId = $(this).parent().parent().parent().children('div.col-md-5').children('select.advisor-engagement-select').children('option:selected').val();
            let getGoogleUrl = $(this).parent().parent().parent().children('div.col-md-5').children('select.advisor-engagement-select').children('option:selected').data('get-google-url');
            let advisorPercentage = $(this).val();


            let json = {
                advisorId: advisorId,
                advisorPercentage: advisorPercentage
            };

            if (advisorId != "" && advisorPercentage != "") {
                advisorTotalPercentage += parseInt(advisorPercentage);
                advisors.push(json);

                if (!advisorsIdIncludes) {
                    advisorsIdIncludes = advisorsIDs.includes(advisorId)
                }

                advisorsIDs.push(advisorId);
            } else {
                advisorComplete = false;
                advisorsIdIncludes = false;
            }
        });


        if (!advisorComplete) {
            $("#advisor-list").append('<p class="error text-danger text-center">Information about advisor are not full or some fields are empty.<p>');
        }




        if (advisorsIdIncludes) {
            $("#advisor-list").append('<p class="error text-danger text-center">You use twice same advisor for this session.<p>')
        } else if (!advisorsIdIncludes || advisorsCount == 0) {
            check++;
        }

        if (advisorTotalPercentage == 100 || advisorsCount == 0) {
            check++;
        } else if ((advisorTotalPercentage < 100 || advisorTotalPercentage > 100) && advisorComplete) {
            $("#advisor-list").append('<p class="error text-danger text-center">Advisor percentage are set wrong.<p>');
        }

        let json = {
            clientName: clientName,
            clientEmail: clientEmail,
            clientPhone: clientPhone,
            clientAddress: clientAddress,
            clientBirthDate: clientBirthDate,
            clientGender: clientGender,
            description: description,
            sessionPrice: sessionPrice,
            bookDateStart: bookDateStart,
            bookDateEnd: bookDateEnd,
            advisors: advisors,
        };

        console.log(json);
        var offset = new Date().getTimezoneOffset();





        if (check == 2) {
            axios.post(url, json).then(function (response) {
                window.location.href = response.data;
            }).catch(function (error) {
                console.log(error);
            });
        }

    });



    $(document).on('submit', '#admin-create-project-form', function (e) {
        e.preventDefault();
        let url = $(this).data('url');
        let name = $('#project-name').val();
        let dateStart = $('#project-date-start').val();
        let dateEnd = $('#project-date-end').val();
        let description = $('#project-description').val();
        let price = $('#project-price').val();

        let advisors = [];
        let advisorsIDs = [];
        let advisorsIdIncludes = false;
        let advisorsCount = 0;
        let advisorComplete = true;
        let check = 0;
        $('.error').remove();
        let advisorTotalPercentage = 0;

        $('.advisor-engagement-percentage').each(function () {
            advisorsCount++;
            let advisorId = $(this).parent().parent().parent().children('div.col-md-5').children('select.advisor-engagement-select').children('option:selected').val();
            let advisorPercentage = $(this).val();

            let json = {
                advisorId: advisorId,
                advisorPercentage: advisorPercentage
            };
            if (advisorId != "" && advisorPercentage != "") {
                advisorTotalPercentage += parseInt(advisorPercentage);
                advisors.push(json);
                if (!advisorsIdIncludes) {
                    advisorsIdIncludes = advisorsIDs.includes(advisorId)
                }
                advisorsIDs.push(advisorId);
            } else {
                advisorComplete = false;
                advisorsIdIncludes = false;
            }
        });


        if (!advisorComplete) {
            $("#advisor-list").append('<p class="error text-danger text-center">Information about advisor are not full or some fields are empty.<p>');
        }

        if (advisorsIdIncludes) {
            $("#advisor-list").append('<p class="error text-danger text-center">You use twice same advisor for this session.<p>')
        } else if (!advisorsIdIncludes || advisorsCount == 0) {
            check++;
        }

        if (advisorsCount == 0) {
            $("#advisor-list").append('<p class="error text-danger text-center">You must appoint at least one project advisor.<p>');
        } else {
            check++;
        }

        if (advisorTotalPercentage == 100 || advisorsCount == 0) {
            check++;
        } else if ((advisorTotalPercentage < 100 || advisorTotalPercentage > 100) && advisorComplete) {
            $("#advisor-list").append('<p class="error text-danger text-center">Advisor percentage are set wrong.<p>');
        }

        let json = {
            name: name,
            dateStart: dateStart,
            dateEnd: dateEnd,
            description: description,
            advisors: advisors,
            price: price
        };

        if (check == 3) {
            axios.post(url, json).then(function (response) {
                location.reload();
            }).catch(function (error) {
                console.log(error);
            });
        }
    });

    $(document).on('click', '[data-target="#calendar-event-options"]', function (e) {
        let id = $(this).children('[data-id]').data('id');
        let targetId = $(this).data('target');
        let eventGetUrl = $(targetId).data('get-event-url');
        let eventDestroyUrl = $(targetId).data('destroy-event-url');
        let eventUpdateUrl = $(targetId).data('update-event-url');
        eventGetUrl += "/" + id;
        eventDestroyUrl += "/" + id;
        eventUpdateUrl += "/" + id;

        axios.get(eventGetUrl).then(function (response) {
            if (response.data.status == "private") {
                $('#edit-private-info').hide();
                $('#edit-private-info-alert').show();
            } else {
                let dateStart = new Date(response.data.started_at)
                dateStart = formatDate(dateStart);

                let dateEnd = new Date(response.data.ended_at)
                dateEnd = formatDate(dateEnd);


                moment().format('[The time is] h:mm:ss a');
                $('#edit-private-info').show();
                $('#edit-event-delete-button').data('url', eventDestroyUrl);
                $('#edit-event-form').data('url', eventUpdateUrl);
                $('#edit-event-date-time-start-input').val(dateStart);
                $('#edit-event-date-time-end-input').val(dateEnd);
                $('#edit-private-info-alert').hide();
                $('#edit-event-title').val(response.data.name);
                $('#edit-event-description').text(response.data.description);
            }
            console.log(response.data);
        }).catch(function (error) {
            console.log(error);
        });

    });

    $(document).on('click', '#edit-event-delete-button', function (e) {
        let url = $(this).data('url');
        axios.delete(url).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });

    $(document).on('submit', '#edit-event-form', function (e) {
        e.preventDefault();
        let url = $(this).data('url');
        let title = $('#edit-event-title').val();
        let dateTimeStart = $('#edit-event-date-time-start-input').val();
        let dateTimeEnd = $('#edit-event-date-time-end-input').val();
        let description = $('#edit-event-description').val();

        let json = {
            title: title,
            dateTimeStart: dateTimeStart,
            dateTimeEnd: dateTimeEnd,
            description: description
        };

        axios.put(url, json).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });

    $('#edit-event-delete-modal-trigger').click(function () {
        $('#edit-private-info').hide();
        $('#delete-event-modal-form').removeAttr('hidden');
    });

    $('#delete-event-modal-form-close').click(function () {
        $('#edit-private-info').show();
        $('#delete-event-modal-form').attr('hidden', true);
    });

    $('#add-project-raport-form').submit(function (e) {
        e.preventDefault();
        let id = $(this).data('id');
        let url = $(this).data('url');
        let description = $('#add-project-raport-form-description').val();

        let json = {
            id: id,
            description: description
        };

        axios.post(url, json).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });
    });

    $('.delete-project-btn').click(function () {
        let url = $(this).data('url');

        axios.delete(url).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });

    });


    $('#add-email-type-form').submit(function (e) {
        e.preventDefault();
        let url = $(this).data('url');
        let name = $('#add-email-type-form-general-name').val();
        let namesTrans = [];
        $('.add-email-type-form-trans-name').each(function(){
            let langId = $(this).data('lang-id');
            let name = $(this).val();
            let json = {
                langId: langId,
                name: name
            };
            namesTrans.push(json);
        });

        let json = {
            name: name,
            namesTrans: namesTrans
        };

        console.log(url);
       

        axios.post(url, json).then(function (response) {
            location.reload();
        }).catch(function (error) {
            console.log(error);
        });

    });







});
