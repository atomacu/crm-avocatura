<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('command', function () {
	
	/* php artisan migrate */
    \Artisan::call('migrate:fresh --seed');
    dd("Done");
});

Route::group(['middleware' => ['role','share']], function(){
   
    Auth::routes();
    Route::resource('admin', 'Admin\AdminController');
    Route::get('/admin-index/{lang?}', 'Admin\AdminController@index')->name('admin.index');


    Route::resource('admin-mail', 'Admin\Mail\MailController');
    Route::get('/admin-mail-index/{lang?}', 'Admin\Mail\MailController@index')->name('admin-mail.index');
    
    Route::resource('group-email-template', 'Admin\Mail\GroupTemplateController');
    Route::get('/group-email-template-index/{lang?}', 'Admin\Mail\GroupTemplateController@index')->name('group-email-template.index');
    Route::put('/updateInUse/{id}', 'Admin\Mail\GroupTemplateController@updateInUse')->name('group-email-template.updateInUse');   


    Route::resource('email-template', 'Admin\Mail\TemplateController');


    Route::resource('admin-contact-form', 'Admin\ContactForm\ContactFormController');
    Route::get('/admin-contact-form-index/{lang?}', 'Admin\ContactForm\ContactFormController@index')->name('admin-contact-form.index');
    Route::get('/admin-contact-form-show/{id}/{lang?}', 'Admin\ContactForm\ContactFormController@show')->name('admin-contact-form.show');


    Route::resource('admin-user-management', 'Admin\UserManagement');
    Route::get('/admin-user-management-index/{lang?}', 'Admin\UserManagement@index')->name('admin-user-management.index');

    Route::resource('admin-general-settings', 'Admin\GeneralSettings');
    Route::get('/admin-general-settings-index/{lang?}', 'Admin\GeneralSettings@index')->name('admin-general-settings.index');

    Route::resource('admin-gender', 'Admin\GenderController');
    
    Route::resource('sessions', 'Client\SessionController');
    Route::get('/sessions/create/{lang?}/{id?}', 'Client\SessionController@create')->name('sessions.create');

    Route::resource('contact-form', 'Client\ContactFormController');
    



    
    Route::resource('admin-new-sessions', 'Admin\Session\NewSessions');
    Route::get('/admin-new-sessions-index/{lang?}', 'Admin\Session\NewSessions@index')->name('admin-new-sessions.index');
    Route::get('/admin-new-sessions-show/{id?}/{lang?}', 'Admin\Session\NewSessions@show')->name('admin-new-sessions.show');
    Route::get('/admin-new-sessions-new-advisor', 'Admin\Session\NewSessions@newAdvisor')->name('admin-new-sessions.newAdvisor');
    Route::put('/cancelNewSession/{id}', 'Admin\Session\NewSessions@cancelNewSession')->name('admin-new-sessions.cancelNewSession');
    Route::get('/get-user-google/{id}', 'Admin\Session\NewSessions@getUserGoogle')->name('admin-new-sessions.getUserGoogle');
    
    

    
    Route::resource('admin-sessions', 'Admin\Session\SessionController');
    Route::put('/endSession/{id}', 'Admin\Session\SessionController@endSession')->name('admin-sessions.endSession');
    Route::put('/cancelSession/{id}', 'Admin\Session\SessionController@cancelSession')->name('admin-sessions.cancelSession');
    
    
    
    Route::resource('admin-finished-sessions', 'Admin\Session\FinishedSessions');
    Route::get('/admin-finished-sessions-index/{lang?}', 'Admin\Session\FinishedSessions@index')->name('admin-finished-sessions.index');
    Route::get('/admin-finished-sessions-show/{id?}/{lang?}', 'Admin\Session\FinishedSessions@show')->name('admin-finished-sessions.show');


    
    Route::resource('admin-new-projects', 'Admin\Project\NewProjectsController');
    Route::get('/admin-new-projects-index/{lang?}', 'Admin\Project\NewProjectsController@index')->name('admin-new-projects.index');
    
    Route::resource('admin-projects', 'Admin\Project\ProjectsController');
    Route::get('/admin-projects-index/{lang?}', 'Admin\Project\ProjectsController@index')->name('admin-projects.index');
    Route::get('/admin-projects-show/{id?}/{lang?}', 'Admin\Project\ProjectsController@show')->name('admin-projects.show');
    Route::get('/admin-projects-new-advisor', 'Admin\Project\ProjectsController@newAdvisor')->name('admin-projects.newAdvisor');
    Route::post('/admin-projects-add-raport', 'Admin\Project\ProjectsController@addRaport')->name('admin-projects.addRaport');
    
    
    Route::resource('admin-tasks', 'Admin\TasksController');
    Route::get('/admin-tasks-index/{lang?}', 'Admin\TasksController@index')->name('admin-tasks.index');



    Route::resource('admin-contact', 'Admin\Contacts\ContactCompanyController');
    Route::get('/admin-contact-index/{lang?}', 'Admin\Contacts\ContactCompanyController@index')->name('admin-contact.index');

    Route::resource('admin-contact-settings', 'Admin\Contacts\ContactSettingsController');
    Route::get('/admin-contact-settings-index/{lang?}', 'Admin\Contacts\ContactSettingsController@index')->name('admin-contact-settings.index');
    
    Route::resource('admin-contact-settings', 'Admin\Contacts\EmailTypeController');
    


    Route::resource('admin-calendar', 'Admin\Calendar\CalendarController');
    Route::get('/admin-calendar-index/{lang?}', 'Admin\Calendar\CalendarController@index')->name('admin-calendar.index');
    
    
    Route::resource('admin-google-controller', 'Admin\Calendar\GoogleController');
    Route::get('/admin-calendar-google-store/oauth', 'Admin\Calendar\GoogleController@store')->name('admin-google-controller.store');
    Route::delete('/admin-calendar-google-store/{googleAccount}', 'Admin\Calendar\GoogleController@destroy')->name('admin-google-controller.destroy');


    Route::resource('admin-event', 'Admin\Calendar\EventController');
    Route::get('/admin-event-edit/{id?}', 'Admin\Calendar\EventController@edit')->name('admin-event.edit');
    Route::delete('/admin-event-destroy/{id?}', 'Admin\Calendar\EventController@destroy')->name('admin-event.destroy');
    Route::put('/admin-event-update/{id?}', 'Admin\Calendar\EventController@update')->name('admin-event.update');
   
    

    Route::resource('moderator', 'Moderator\ModeratorController');
    Route::get('/moderator-index/{lang?}', 'Moderator\ModeratorController@index')->name('moderator.index');
    
    


    
    Route::resource('consultant', 'Consultant\ConsultantController');
    Route::get('/consultant-index/{lang?}', 'Consultant\ConsultantController@index')->name('consultant.index');



    Route::resource('consultant-sessions', 'Consultant\SessionsController');
    Route::get('/consultant-sessions-show/{id?}/{lang?}', 'Consultant\SessionsController@show')->name('consultant-sessions.show');
    Route::get('/consultant-sessions-create/{lang?}', 'Consultant\SessionsController@create')->name('consultant-sessions.create');
    Route::put('/endConsultantSession/{id}', 'Consultant\SessionsController@endSession')->name('consultant-sessions.endSession');
    Route::put('/cancelConsultantSession/{id}', 'Consultant\SessionsController@cancelSession')->name('consultant-sessions.cancelSession');
   
    Route::resource('consultant-finished-sessions', 'Consultant\FinishedSessionsController');
    Route::get('/consultant-finished-sessions-index/{lang?}', 'Consultant\FinishedSessionsController@index')->name('consultant-finished-sessions.index');
    Route::get('/consultant-finished-sessions-show/{id?}/{lang?}', 'Consultant\FinishedSessionsController@show')->name('consultant-finished-sessions.show');
    

    

    
    Route::get('/home/{lang?}', 'HomeController@index')->name('home');
    Route::get('/{lang?}', 'PagesController@wellcome')->name('wellcome');
});


      
        
      