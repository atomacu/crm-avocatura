<?php

use App\Role;
use App\Langs;
use App\UserRole;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // store($name,$description,$defaultAccess=0,$defaultPage,$setDefault=0)
        $roleId = Role::store('Admin',"Poate face tot ce vrea.",1,'admin',0);
        Role::store('Moderator',"Poate face tot ce vrea.",1,'Moderator',0);
        $roleId1 =Role::store('Consultant',"Poate face tot ce vrea.",1,'consultant',0);
      

        $userId = DB::table('users')->insertGetId([
            'name' => "Tomacu Alexandru",
            'email' => "atomacu@gmail.com",
            'phone' => "1234567890",
            'password' => Hash::make('1234567890'),
        ]);
        UserRole::store($userId,$roleId);



        $userId1 = DB::table('users')->insertGetId([
            'name' => "Tomacu1 Alexandru",
            'email' => "atomacu1@gmail.com",
            'phone' => "1234567890",
            'password' => Hash::make('1234567890'),
        ]);
        UserRole::store($userId1,$roleId1);

        $userId2 = DB::table('users')->insertGetId([
            'name' => "Tomacu2 Alexandru",
            'email' => "atomacu2@gmail.com",
            'phone' => "1234567890",
            'password' => Hash::make('1234567890'),
        ]);
        UserRole::store($userId2,$roleId1);

        $userId3 = DB::table('users')->insertGetId([
            'name' => "Tomacu3 Alexandru",
            'email' => "atomacu3@gmail.com",
            'phone' => "1234567890",
            'password' => Hash::make('1234567890'),
        ]);
        UserRole::store($userId3,$roleId1);

        Langs::create([
            'name' => "Ro",
            'system_name' => 'ro'
        ]);
        Langs::create([
            'name' => "Ru",
            'system_name' => 'ru'
        ]);
    }
}
