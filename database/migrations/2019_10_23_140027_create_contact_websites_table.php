<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactWebsitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_websites', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('contacts_id')->unsigned()->nullable();
            $table->foreign('contacts_id')->references('id')->on('contacts')->onDelete('cascade');
            $table->bigInteger('website_type_id')->unsigned()->nullable();
            $table->foreign('website_type_id')->references('id')->on('contact_website_types')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_websites');
    }
}
