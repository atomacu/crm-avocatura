<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutomatResponsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('automat_responses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('draft_response_id')->unsigned();
            $table->foreign('draft_response_id')->references('id')->on('draft_responses')->onDelete('cascade');
            $table->bigInteger('form_id')->unsigned();
            $table->foreign('form_id')->references('id')->on('contact_forms')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('automat_responses');
    }
}
