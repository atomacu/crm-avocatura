@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">
            <h4>
                <i class="fas fa-umbrella mr-2"></i>New projects
            </h4>
        </div>
    </div>
    <hr>
    <div class="row text-white mx-1 bg-dark">
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Project name
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Advisors
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Start date
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            End date
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Price
        </div>
    </div>

</div>
@endsection
