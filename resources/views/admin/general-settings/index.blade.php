@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <h4>
        <i class="fas fa-cogs mr-2"></i>General settings
    </h4>
    <hr>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" href="#home">Geder settings</a>
        </li>
    </ul>
    <div class="row justify-content-center mt-3">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary btn-block" data-toggle="modal"
                data-target="#add-gender-type-modal">
                <i class="fas fa-plus"></i>
            </button>

            <!-- Modal -->
            <div class="modal fade" id="add-gender-type-modal" tabindex="-1" role="dialog"
                aria-labelledby="add-gender-type-modal-label" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="add-gender-type-modal-label">Add gender type</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form data-url="{{ route('admin-gender.store') }}" id="add-gender-type-form">
                                <div class="row mt-2 justify-content-center">
                                    <div class="col-md-10">
                                        <input type="text" id="add-gender-type-general-name" placeholder="General name"
                                            class="form-control">
                                    </div>
                                </div>
                                @foreach ($langs as $lang)
                                <div class="row mt-2 justify-content-center">
                                    <div class="col-md-10">
                                        <input type="text" data-lang-id="{{$lang->id}}"
                                            placeholder="Gender name in {{$lang->name}}"
                                            class="gender-name-trans form-control">
                                    </div>
                                </div>
                                @endforeach
                                <div class="row mt-2 justify-content-center">
                                    <div class="col-md-10">
                                        <button class="btn btn-success btn-block">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="mt-4 row mx-1 bg-dark text-white bg-light-hover">
        <div class="pointer p-2 text-center border-bottom col-md-2">
            Gender group name
        </div>
        @foreach ($langs as $lang)
        <div class="pointer p-2 text-center border-bottom col-md">
            {{$lang->name}}
        </div>
        @endforeach
    </div>

    @foreach ($genders as $gender)
    <div class="row mx-1 bg-light bg-light-hover">
        <div class="pointer h7 p-2 text-center border-bottom col-md-2">
            {{$gender->name}}
        </div>
        @foreach ($gender->trans as $trans)
        <div class="pointer h7 p-2 text-center border-bottom col-md">
            {{$trans->name}}
        </div>
        @endforeach
    </div>
    @endforeach


</div>
@endsection
