@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <h4>
                <i class="fas fa-tasks"></i>  Tasks
            </h4>
        </div>
        <div class="col-md-2 ml-auto">
            <button class="btn btn-primary btn-block">
                <i class="fas fa-plus mr-2"></i>New Task
            </button>
        </div>
    </div>
    <hr class="my-1">
</div>
@endsection
