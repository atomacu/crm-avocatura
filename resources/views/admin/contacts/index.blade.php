@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <h4>
                <i class="fas fa-address-card mr-2"></i>Contact
            </h4>
        </div>
        <div class="col-md-2 ml-auto">
            <button class="btn btn-primary btn-block">
                <i class="fas fa-plus mr-2"></i>New contact
            </button>
        </div>
        <div class="col-md-1">
            <a class="h3 text-dark" href="{{route('admin-contact-settings.index',App::getLocale())}}">
                <i class="fas fa-cogs pointer"></i>
            </a>
        </div>
    </div>
    <hr class="my-1">
</div>
@endsection
