@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6">
            <h4>
                <i class="fas fa-cogs mr-2"></i>Contact settings
            </h4>
        </div>
        <div class="col-md-2 ml-auto">

        </div>
    </div>
    <hr class="my-1">

    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs" id="settings-types" role="settings-types-list">
                <li class="nav-item">
                    <a class="nav-link active" id="types-tab" data-toggle="tab" href="#types" role="tab"
                        aria-controls="types" aria-selected="true">Types</a>
                </li>
            </ul>
            <div class="tab-content" id="settings-types-content">
                <div class="tab-pane fade show active" id="types" role="tabpanel" aria-labelledby="types-tab">
                    <div class="row justify-content-center my-4">
                        <div class="col-md-10">
                            <div class="accordion" id="accordionExample">
                                <div class="card">
                                    <div class="card-header" id="email-types">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse"
                                                data-target="#collapseOne" aria-expanded="true"
                                                aria-controls="collapseOne">
                                                Email types
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="collapseOne" class="collapse show" aria-labelledby="email-types"
                                        data-parent="#accordionExample">
                                        <div class="card-body p-0">
                                            <div class="row mb-2 justify-content-center">
                                                <div class="col-md-12">
                                                    <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#add-email-type-modal">
                                                        <i class="fas fa-plus mr-2"></i>Add type
                                                    </button>

                                                    <div class="modal fade" id="add-email-type-modal" tabindex="-1" role="dialog" aria-labelledby="add-email-type-modal-label" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="add-email-type-modal-label">
                                                                        New email type
                                                                    </h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                        <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <div class="modal-body">
                                                                    <form data-url="{{route('admin-contact-settings.store')}}" id="add-email-type-form">
                                                                        <div class="row mb-2 justify-content-center">
                                                                            <div class="col-md-10">
                                                                                <input type="text" id="add-email-type-form-general-name" placeholder="Type general name" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                        @foreach ($langs as $lang)
                                                                        <div class="row mb-2 justify-content-center">
                                                                            <div class="col-md-10">
                                                                                <input type="text" data-lang-id="{{$lang->id}}" placeholder="Type name in {{$lang->name}}" class="add-email-type-form-trans-name form-control">
                                                                            </div>
                                                                        </div>
                                                                        @endforeach
                                                                        <div class="row mb-2 justify-content-center">
                                                                            <div class="col-md-10">
                                                                                <button type="submit" class="btn btn-success btn-block">
                                                                                    Add
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </form>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row text-white mx-1 bg-dark">
                                                        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
                                                            Project name
                                                        </div>
                                                        @foreach ($langs as $lang)
                                                            <div class="pointer a-redirect p-2 text-center border-bottom col-md">
                                                                    {{$lang->name}}
                                                            </div>
                                                        @endforeach
                                                        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
                                                            Edit
                                                        </div>
                                                        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
                                                            Delete
                                                        </div>
                                                    </div>
                                                   
                                                    @foreach ($contactEmailTypes as $emailType)
                                                        <div data-redirect-url="#" class="row mx-1 bg-light bg-light-hover">
                                                            <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
                                                                {{ $emailType->name }}
                                                            </div>
                                                            @foreach ($langs as $lang)
                                                                <div class="h7 p-2 text-center border-bottom col-md">
                                                                    @if($emailType->trans($lang->system_name) !== null )  
                                                                        {{ $emailType->trans($lang->system_name)->name}} 
                                                                    @endif
                                                                </div>
                                                            @endforeach
                                                            <div class="h7 p-2 text-center border-bottom col-md">
                                                                <i class="fas fa-edit text-success"></i>
                                                            </div>
                                                            <div class="h7 p-2 text-center border-bottom col-md">
                                                                <i class="fas fa-trash-alt text-danger"></i>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="phone-types">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseTwo" aria-expanded="false"
                                                aria-controls="collapseTwo">
                                                Phone types
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="phone-types"
                                        data-parent="#accordionExample">
                                        <div class="card-body">
                                            rtger
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header" id="website-types">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseThree" aria-expanded="false"
                                                aria-controls="collapseThree">
                                                Website types
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="website-types"
                                        data-parent="#accordionExample">
                                        <div class="card-body">
                                            sdfsdf
                                        </div>
                                    </div>
                                </div>

                                <div class="card">
                                    <div class="card-header" id="addres-types">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                                                data-target="#collapseThree" aria-expanded="false"
                                                aria-controls="collapseThree">
                                                Address types
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="collapseThree" class="collapse" aria-labelledby="addres-types"
                                        data-parent="#accordionExample">
                                        <div class="card-body">
                                            tyjhgdfd
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
