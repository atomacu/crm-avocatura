@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card border-0 border-bottom">
                <div class="card-header border-0 d-flex justify-content-between align-items-center">
                    <span><i class="fab fa-google mr-2"></i>Google Accounts</span>
                    <a class="btn btn-primary btn-sm" href="{{ route('admin-google-controller.store') }}">
                        <i class="fab fa-google-plus-g mr-2"></i>Add account
                    </a>
                </div>

                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        @forelse (Auth::user()->googleAccounts as $account)
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            <span>{{ $account->name }}</span>
                            <form action="{{ route('admin-google-controller.destroy', $account) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}

                                <button type="submit" class="btn btn-outline-secondary btn-sm">
                                    delete
                                </button>
                            </form>
                        </li>
                        @empty
                        <li class="list-group-item">
                            No google accounts.
                        </li>
                        @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="my-4 card card-primary">
        <div class="card-body">
            {!! $calendar_details->calendar() !!}
        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" data-update-event-url="{{ route('admin-event.update') }}"
        data-destroy-event-url="{{ route('admin-event.destroy') }}" data-get-event-url="{{ route('admin-event.edit') }}"
        id="calendar-event-options" tabindex="-1" role="dialog" aria-labelledby="calendar-event-options-label"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="calendar-event-options-label">Event management</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="edit-private-info">
                        <form id="edit-event-form">
                            <div class="row mb-2 justify-content-center">
                                <div class="col-md-10">
                                    <input class="form-control" id="edit-event-title" placeholder="Event title"
                                        type="text">
                                </div>
                            </div>
                            <div class="row mb-2 justify-content-center">
                                <div class="col-md-10">
                                    <div class="form-group mb-0">
                                        <div class="input-group date" id="edit-event-date-time-start"
                                            data-target-input="nearest">
                                            <input type="text" placeholder="Start"
                                                class="form-control datetimepicker-input" data-toggle="datetimepicker"
                                                id="edit-event-date-time-start-input"
                                                data-target="#edit-event-date-time-start" />
                                            <div class="input-group-append" data-target="#edit-event-date-time-start"
                                                data-toggle="datetimepicker">
                                                <div class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-2 justify-content-center">
                                <div class="col-md-10">
                                    <div class="form-group mb-0">
                                        <div class="input-group date" id="edit-event-date-time-end"
                                            data-target-input="nearest">
                                            <input type="text" class="form-control datetimepicker-input"
                                                placeholder="End" data-toggle="datetimepicker"
                                                id="edit-event-date-time-end-input"
                                                data-target="#edit-event-date-time-end" />
                                            <div class="input-group-append" data-target="#edit-event-date-time-end"
                                                data-toggle="datetimepicker">
                                                <div class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="justify-content-center mb-2 row">
                                <div class="col-md-10">
                                    <textarea id="edit-event-description" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="row mb-2 justify-content-center">
                                <div class="col-md-10">
                                    <button class="btn btn-success btn-block">Save</button>
                                </div>
                            </div>
                        </form>
                        <hr>
                        <div class="row mb-2 justify-content-center">
                            <div class="col-md-10">
                                <button class="btn btn-danger btn-block" id="edit-event-delete-modal-trigger" >
                                    Delete
                                </button>
                            </div>
                        </div>


                    </div>

                    <div hidden id="delete-event-modal-form" class="row justify-content-center">
                            <h5 class="mb-4 modal-title text-center w-100">
                                Are you sure that you want to cancel this session?
                            </h5>
                            <div class="col-md-5">
                                <button type="button" class="btn btn-block btn-secondary" id="delete-event-modal-form-close">
                                    Close
                                </button>
                            </div>
                            <div class="col-md-5">
                                <button id="edit-event-delete-button" class="btn btn-danger btn-block">
                                    Delete
                                </button>
                            </div>
                    </div>
                    <div id="edit-private-info-alert">
                        <div class="justify-content-center row">
                            <div class="col-md-10">
                                <div class="alert alert-danger" role="alert">
                                    This is private event.
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
