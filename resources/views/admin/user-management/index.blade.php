@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <h4>
        <i class="fas fa-users mr-2"></i>User Management
    </h4>
    <hr>
    @foreach ($users as $user)
    <div class="row mx-1 bg-light bg-light-hover">
        <div class="pointer h7 p-2 text-center border-bottom col-md-1">
            {{$user->id}}
        </div>
        <div class="pointer h7 p-2 text-center border-bottom col-md">
            {{$user->name}}
        </div>
        <div class="pointer h7 p-2 text-center border-bottom col-md">
            {{$user->email}}
        </div>
        <div class="pointer h7 p-2 text-center border-bottom col-md">
            {{$user->userRole->role->name}}
        </div>
    </div>
    @endforeach
</div>

@endsection
