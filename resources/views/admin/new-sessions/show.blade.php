@extends('layouts.admin')

@section('content')
<div class="container-fluid mb-5">
    <h4>
        <i class="far fa-folder-open mr-2"></i>New sessions
    </h4>
    <hr>

    <form data-url="{{ route('admin-new-sessions.update',$mail->id) }}" id="session-confirm-form">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h4>Client Info</h4>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-5">
                <label for="session-client-infor-name">Client name</label>
                <input value="{{$mail->name}}" id="session-client-infor-name" class="form-control" type="text">
            </div>
            <div class="col-md-5">
                <label for="session-client-infor-email">Email address</label>
                <input value="{{$mail->email}}" id="session-client-infor-email" class="form-control" type="text">
            </div>
        </div>
        <div class="row mt-2 justify-content-center">
            <div class="col-md-5">
                <label for="session-client-infor-phone">Phone number</label>
                <input value="{{$mail->phone}}" id="session-client-infor-phone" class="form-control" type="text">
            </div>
            <div class="col-md-5">
                <label for="session-client-infor-address">Address</label>
                <input value="{{$mail->address}}" id="session-client-infor-address" placeholder="Address"
                    class="form-control" type="text">
            </div>
        </div>
        <div class="row mt-2 justify-content-center">
            <div class="col-md-5">
                <label for="session-client-infor-birth_date">Birth date</label>
                <input value="{{$mail->birth_date}}" id="session-client-infor-birth_date" placeholder="Birth date"
                    class="form-control" type="date">
            </div>
            <div class="col-md-5">
                <label for="session-client-infor-gender">Gender</label>
                <select name="" id="session-client-infor-gender" class="form-control rounded-0" id="my-select">
                    <option value="" selected disabled>Gender</option>
                    @foreach ($genders as $gender)
                    <option value="{{$gender->id}}" @if($gender->id == $mail->gender_id) selected
                        @endif>{{$gender->genderTrans(App::getLocale())->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row mt-2 justify-content-center">
            <div class="col-md-10">
                <label for="exampleInputEmail1">Contact message</label>
                <p>{{$mail->description}}</p>
            </div>
        </div>
        @if(isset($mail->response))
        <div class="row mt-3 justify-content-center">
            <div class="col-md-10">
                <h4 class="m-0">Manager's response</h4>
            </div>
        </div>

        <div class="row mt-1 mb-3 justify-content-center">
            <div class="col-md-10">
                <p class="my-1"><span class="font-weight-bold mr-2">Subject:</span>{{$mail->response->title}}</p>
                <p class="my-1"><span class="font-weight-bold mr-2">Email:</span></p>
                {!!$mail->response->description!!}
            </div>
        </div>
        @endif

        @if(isset($mail->session))
        <div class="row mt-5 justify-content-center">
            <div class="col-md-10">
                <h4>Information about the reserved session</h4>
            </div>
        </div>

        <div class="row mt-2 justify-content-center">
            <div class="col-md-10">
                <div class="row mx-1 bg-dark text-white">
                    <div class="pointer p-2 text-center border-bottom col-md">
                        Date and time when the session starts
                    </div>
                    <div class="pointer p-2 text-center border-bottom col-md">
                        Date and time when the session ends
                    </div>
                </div>
                <div class="row mb-4 mx-1 bg-light bg-light-hover">
                    <div class="pointer p-2 text-center border-bottom col-md">
                        {{$mail->session->date_time_start}}
                    </div>
                    <div class="pointer p-2 text-center border-bottom col-md">
                        {{$mail->session->date_time_end}}
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h4>Advisors Enrollment</h4>
            </div>
        </div>

        <div class="row mt-2 justify-content-center">
            <div id="advisor-list" class="col-md-10">
                @foreach ($mail->session->consultants as $consultant)
                <div class="mb-2 row">
                    <div class="col-md-5">
                        <select name="" id="" class="advisor-engagement-select form-control">
                            @foreach ($users as $user)
                            <option value="{{$user->id}}" @if($user->id == $consultant->user_id) selected
                                @endif>{{$user->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group mb-3">
                            <input type="number" value="{{$consultant->engagement_rate}}"
                                class="advisor-engagement-percentage form-control"
                                aria-label="advisor engagement percentage">
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <button id="delete-advisor" class="btn btn-danger btn-block">Delete</button>
                    </div>
                </div>

                @endforeach
            </div>
        </div>


        <div class="row mt-2 justify-content-center">
            <div class="col-md-10">
                <button type="button" data-url="{{route('admin-new-sessions.newAdvisor')}}" id="enrole-consultant-session"
                    class="btn btn-primary btn-block">
                    <i class="fas fa-plus"></i>
                </button>
            </div>
        </div>

        <div class="row mt-4 justify-content-center">
            <div class="col-md-10">
                <h4>Session price</h4>
            </div>
        </div>

        <div class="row mt-1 justify-content-center">
            <div class="col-md-10">
                <div class="input-group mb-3">
                    <input type="number" class="form-control" id="session-price" value="{{$mail->session->price}}">
                    <div class="input-group-append">
                        <span class="input-group-text">€</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-4 justify-content-center">
            <div class="mt-2 col-md-5">
                <button type="submit" data-next="1" class="btn btn-success btn-block">Save</button>
            </div>
            <div class="mt-2 col-md-5">
                <button type="button" class="btn btn-danger btn-block" data-toggle="modal"
                    data-target="#cancel-session-modal">Cancel</button>

                <div class="modal fade" id="cancel-session-modal" tabindex="-1" role="dialog"
                    aria-labelledby="cancel-session-modal-label" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title text-center w-100" id="cancel-session-modal-label">
                                    Are you sure that you want to cancel this session?
                                </h5>

                            </div>
                            <div class="modal-body">
                                <div class="row justify-content-center">
                                    <div class="col-md-5">
                                        <button type="button" class="btn btn-secondary btn-block"
                                            data-dismiss="modal">Close</button>
                                    </div>
                                    <div class="col-md-5">
                                        <button type="button" id="cancel-new-session-confirm-button" data-url="{{ route('admin-new-sessions.cancelNewSession',$mail->id) }}" class="btn btn-danger btn-block">Cancel Session</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>
@endsection
