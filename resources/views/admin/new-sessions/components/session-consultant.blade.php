<div class="mb-2 row">
    <div class="col-md-5">
        <select name="" id="" class="advisor-engagement-select form-control">
            <option value="" selected disabled>Select session advisor</option>
            @foreach ($users as $user)
                <option value="{{$user->id}}">{{$user->name}}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-2">
        <div class="input-group mb-3">
            <input type="number" class="advisor-engagement-percentage form-control" aria-label="advisor engagement percentage">
            <div class="input-group-append">
                <span class="input-group-text">%</span>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <button id="delete-advisor" class="btn btn-danger btn-block">Delete</button>
    </div>
</div>
