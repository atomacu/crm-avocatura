@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <h4>
        <i class="far fa-folder-open mr-2"></i>New sessions
    </h4>
    <hr>
    <div class="row text-white mx-1 bg-dark">
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Client name
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Client email
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Client phone
        </div>
    </div>
    @foreach ($mails as $mail)
        @if($mail->session->price==null || count($mail->session->consultants) == 0)
            <div data-redirect-url="{{route('admin-new-sessions.show',['id'=>$mail->id,'lang'=>App::getLocale()])}}"
                class="row mx-1 bg-light bg-light-hover">
                <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
                    {{$mail->name}}
                </div>
                <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
                    {{$mail->email}}
                </div>
                <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
                    {{$mail->phone}}
                </div>
            </div>
        @endif
    @endforeach
</div>
@endsection
