@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">
            <h4>
                <i class="fas fa-project-diagram mr-2"></i>Projects
            </h4>
        </div>

    </div>
    <hr>
    <div class="row text-white mx-1 bg-dark">
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Project name
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Advisors
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Start date
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            End date
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Progress
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Price
        </div>
    </div>

    <div class="row mx-1 bg-light bg-light-hover">
        <div class="h7 p-2 text-center border-bottom col-md">
            {{$project->name}}
        </div>
        <div class="h7 p-2 text-center border-bottom col-md">
            @foreach ($project->advisors as $advisor)
            <div class="row my-1">
                <div class="col-md-12 pointer" data-toggle="modal" data-target="#advisor-info-modal">
                    {{$advisor->user->name}}<i class="fas fa-info-circle ml-2"></i>

                    <div class="modal fade" id="advisor-info-modal" tabindex="-1" role="dialog"
                        aria-labelledby="advisor-info-modal-label" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="advisor-info-modal-label">{{$advisor->user->name}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>Email: {{$advisor->user->email}} </p>
                                    <p>Email: {{$advisor->user->phone}} </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="h7 p-2 text-center border-bottom col-md">
            {{date_create($project->date_start)->format('d/m/Y')}}
        </div>
        <div class="h7 p-2 text-center border-bottom col-md">
            {{date_create($project->date_end)->format('d/m/Y')}}
        </div>
        <div class="h7 p-2 text-center border-bottom col-md">
            <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: {{round($project->getPercentage())}}%;"
                    aria-valuenow="{{round($project->getPercentage())}}" aria-valuemin="0" aria-valuemax="100">
                    {{round($project->getPercentage())}} %
                </div>
            </div>
        </div>
        <div class="h7 p-2 text-center border-bottom col-md">
            {{$project->price}}
        </div>
    </div>
    <div class="row mt-3 justify-content-center">
        <div class="col-md-12">
            <b>Description:</b>
        </div>
    </div>
    <div class="row mt-3 justify-content-center">
        <div class="col-md-11">
            {{$project->description}}
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#add-project-raport-modal">
                <i class="fas fa-plus mr-2"></i>Add raport
            </button>

            <div class="modal fade" id="add-project-raport-modal" tabindex="-1" role="dialog"
                aria-labelledby="add-project-raport-modal-label" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="add-project-raport-modal-label">New raport</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form data-url="{{ route('admin-projects.addRaport') }}" data-id="{{$project->id}}"
                                id="add-project-raport-form">
                                <div class="row mb-2 justify-content-center">
                                    <div class="col-md-10">
                                        <textarea id="add-project-raport-form-description" class="form-control"
                                            required></textarea>
                                    </div>
                                </div>
                                <div class="row mb-2 justify-content-center">
                                    <div class="col-md-10">
                                        <button class="btn btn-primary btn-block">
                                            <i class="fas fa-plus mr-2"></i>Add
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @foreach ($project->projectRaports as $projectRaport)


    <div class="row mx-1 bg-light bg-light-hover">
        <div class="h7 p-2 text-center border-bottom col-md">
            <div class="row my-1">
                <div class="col-md-12 pointer" data-toggle="modal"
                    data-target="#modal-advisor-info-modal-{{$projectRaport->id}}">
                    {{$projectRaport->user->name}}<i class="fas fa-info-circle ml-2"></i>

                    <div class="modal fade" id="modal-advisor-info-modal-{{$projectRaport->id}}" tabindex="-1"
                        role="dialog" aria-labelledby="modal-advisor-info-modal-{{$projectRaport->id}}-label"
                        aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">

                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5 class="modal-title text-center"
                                        id="modal-advisor-info-modal-{{$projectRaport->id}}-label">
                                        {{$projectRaport->user->name}}</h5>
                                    <p>Email: {{$projectRaport->user->email}} </p>
                                    <p>Email: {{$projectRaport->user->phone}} </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="pointer h7 p-2 text-center border-bottom col-md" data-toggle="modal"
            data-target="#raport-project-info-modal-{{$projectRaport->id}}">
            {{$projectRaport->created_at->format('d/m/Y')}}
        </div>

        <div class="pointer h7 p-2 text-center border-bottom col-md" data-toggle="modal"
            data-target="#raport-project-info-modal-{{$projectRaport->id}}">
            <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: {{round($projectRaport->getPercentage())}}%;"
                    aria-valuenow="{{round($projectRaport->getPercentage())}}" aria-valuemin="0" aria-valuemax="100">
                    {{round($projectRaport->getPercentage())}} %</div>
            </div>
        </div>
        <div class="modal fade" id="raport-project-info-modal-{{$projectRaport->id}}" tabindex="-1" role="dialog"
            aria-labelledby="raport-project-info-modal-{{$projectRaport->id}}-label" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="raport-project-info-modal-{{$projectRaport->id}}-label">
                            Raport
                        </h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{$projectRaport->description}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endforeach

</div>
@endsection
