@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">
            <h4>
                <i class="fas fa-project-diagram mr-2"></i>Projects
            </h4>
        </div>
        <div class="col-md-2">
            <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#add-new-project-modal">
                <i class="fas fa-plus mr-2"></i>Create new project
            </button>

            <div class="modal fade" id="add-new-project-modal" tabindex="-1" role="dialog" aria-labelledby="add-new-project-modal-label"
                aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="add-new-project-modal-label">New project</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form data-url="{{route('admin-projects.store')}}" id="admin-create-project-form">
                                <div class="row justify-content-center mb-2">
                                    <div class="col-md-10">
                                        <input type="text" placeholder="Project name" id="project-name"
                                            class="form-control" required>
                                    </div>
                                </div>
                                <div class="row justify-content-center mb-2">
                                    <div class="col-md-10">
                                        <textarea class="form-control" placeholder="Project description"
                                            id="project-description" required></textarea>
                                    </div>
                                </div>
                                <div class="row justify-content-center mb-2">
                                    <div class="col-md-5">
                                        <input type="date" id="project-date-start" class="form-control" required>
                                    </div>
                                    <div class="col-md-5">
                                        <input type="date" id="project-date-end" class="form-control" required>
                                    </div>
                                </div>
                                <div class="row mb-2 justify-content-center">
                                    <div class="col-md-10">
                                        <input type="number" placeholder="Price" id="project-price" class="form-control"
                                            required>
                                    </div>
                                </div>

                                <div class="row mb-2 justify-content-center">
                                    <div id="advisor-list" class="col-md-10">
                                    </div>
                                </div>


                                <div class="row mb-2 justify-content-center">
                                    <div class="col-md-10">
                                        <button type="button" data-url="{{route('admin-projects.newAdvisor')}}"
                                            id="enrole-consultant-session" class="btn btn-primary btn-block">
                                            Enroll advisors
                                        </button>
                                    </div>
                                </div>
                                <hr>
                                <div class="row justify-content-center mb-2">
                                    <div class="col-md-10">
                                        <button class="btn btn-block btn-success">Create project</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <div class="row text-white mx-1 bg-dark">
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Project name
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Advisors
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Start date
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            End date
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Progress
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Price
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Options
        </div>
    </div>
    @foreach ($projects->reverse() as $project)
    <div data-redirect-url="{{route('admin-projects.show',['id'=>$project->id,'lang'=>App::getLocale()])}}"
        class="row mx-1 bg-light bg-light-hover">

        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$project->name}}
        </div>
        <div class="h7 p-2 text-center border-bottom col-md">
            @foreach ($project->advisors as $advisor)
            <div class="row my-1">
                <div class="col-md-12">
                    {{$advisor->user->name}}<i class="fas fa-info-circle ml-2 pointer" data-toggle="modal"
                        data-target="#advisor-info-modal"></i>

                    <div class="modal fade" id="advisor-info-modal" tabindex="-1" role="dialog"
                        aria-labelledby="advisor-info-modal-label" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <h5 class="modal-title text-center" id="advisor-info-modal-label">
                                        {{$advisor->user->name}}</h5>
                                    <p class="mb-1">Email: {{$advisor->user->email}} </p>
                                    <p>Phone: {{$advisor->user->phone}} </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$project->date_start}}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$project->date_end}}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            <div class="progress">
                <div class="progress-bar" role="progressbar" style="width: {{round($project->getPercentage())}}%;"
                    aria-valuenow="{{round($project->getPercentage())}}" aria-valuemin="0" aria-valuemax="100">
                    {{round($project->getPercentage())}}%</div>
            </div>
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$project->price}}
        </div>
        <div class="pointer h7 p-2 text-center border-bottom col-md">
            <div class="row justify-content-center">
                <div class="col-md-3">
                    <i class="fas fa-edit text-success" data-toggle="modal" data-target="#edit-project-modal-{{$project->id}}"></i>
                  
                    <div class="modal fade" id="edit-project-modal-{{$project->id}}" tabindex="-1" role="dialog"
                        aria-labelledby="edit-project-modal-{{$project->id}}-Label" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="edit-project-modal-{{$project->id}}-Label">Edit "{{$project->name}}"</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    ...
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <i class="far fa-trash-alt text-danger" data-toggle="modal" data-target="#delete-project-modal-{{$project->id}}"></i>
                  
                    <div class="modal fade" id="delete-project-modal-{{$project->id}}" tabindex="-1" role="dialog"
                        aria-labelledby="delete-project-modal-{{$project->id}}-label" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title text-center w-100" id="delete-project-modal-{{$project->id}}-label">
                                        Are you sure that you want to delete this project?
                                    </h5>
                                </div>
                                <div class="modal-body">
                                    <div class="row justify-content-center">
                                        <div class="col-md-5">
                                            <button type="button" class="btn btn-secondary btn-block" data-dismiss="modal">Close</button>
                                        </div>
                                        <div class="col-md-5">
                                            <button type="button" data-url="{{route('admin-projects.destroy',$project->id)}}" class="delete-project-btn btn btn-danger btn-block">Delete</button>
                                        </div>
                                    </div>
                                        
                                </div>
                              
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach

</div>
@endsection
