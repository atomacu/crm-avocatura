@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">
            <h4>
                <i class="fas fa-handshake mr-2"></i>Sessions
            </h4>
        </div>
        <div class="col-md-2">
            <a href="{{route('admin-sessions.create',App::getLocale())}}" class="btn btn-primary btn-block"><i class="fas fa-plus mr-2"></i>Create new session</a>
        </div>
    </div>


    <hr class="mt-1">
    <div class="row text-white mx-1 bg-dark">
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Client name
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Advisors
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Seasson date and time start
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Seasson date and time end
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Price
        </div>
    </div>
    @foreach ($mails as $mail)
    @if(count($mail->session->consultants) != 0 && $mail->session->price!=null)
    <div data-redirect-url="{{route('admin-sessions.show',['id'=>$mail->id,'lang'=>App::getLocale()])}}"
        class="row mx-1 bg-light bg-light-hover">

        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$mail->name}}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            @foreach ($mail->session->consultants as $consultant)
            <div class="row">
                <div class="col-md-12">
                    {{$consultant->user->name}}
                </div>
            </div>
            @endforeach
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{ date_format(date_create($mail->session->date_time_start) ,"m/d/Y H:i") }}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{ date_format(date_create($mail->session->date_time_end) ,"m/d/Y H:i")}}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$mail->session->price}} euro
        </div>
    </div>
    @endif
    @endforeach
</div>
@endsection
