@extends('layouts.admin')

@section('content')
<div class="container-fluid mb-5">
    <h4>
        <i class="fas fa-handshake mr-2"></i>Sessions
    </h4>
    <hr>

    <form data-url="{{ route('admin-sessions.store') }}" id="admin-session-add-form">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h4>Client Info</h4>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-5">
                <label for="session-client-infor-name">Client name</label>
                <input id="admin-session-add-client-infor-name" class="form-control" type="text" required>
            </div>
            <div class="col-md-5">
                <label for="session-client-infor-email">Email address</label>
                <input id="admin-session-add-client-infor-email" class="form-control" type="text" required>
            </div>
        </div>
        <div class="row mt-2 justify-content-center">
            <div class="col-md-5">
                <label for="session-client-infor-phone">Phone number</label>
                <input id="admin-session-add-client-infor-phone" class="form-control" type="text" required>
            </div>
            <div class="col-md-5">
                <label for="session-client-infor-address">Address</label>
                <input id="admin-session-add-client-infor-address" placeholder="Address" class="form-control"
                    type="text">
            </div>
        </div>
        <div class="row mt-2 justify-content-center">
            <div class="col-md-5">
                <label for="session-client-infor-birth_date">Birth date</label>
                <input id="admin-session-add-client-infor-birth_date" placeholder="Birth date" class="form-control"
                    type="date">
            </div>
            <div class="col-md-5">
                <label for="session-client-infor-gender">Gender</label>
                <select name="" id="admin-session-add-client-infor-gender" class="form-control rounded-0"
                    id="my-select">
                    <option value="" selected disabled>Gender</option>
                    @foreach ($genders as $gender)
                    <option value="{{$gender->id}}">
                        {{$gender->genderTrans(App::getLocale())->name}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-2 justify-content-center">
            <div class="col-md-10">
                <textarea name="" placeholder="Description" class="form-control" id="editor-1" required></textarea>
            </div>
        </div>
        <div class="row mt-2 justify-content-center">
            <div class="col-md-10">
                <label for="exampleInputEmail1">Contact message</label>

            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-10">
                <h4>Advisors Enrollment</h4>
            </div>
        </div>

        <div class="row mt-2 justify-content-center">
            <div id="advisor-list" class="col-md-10">
            </div>
        </div>


        <div class="row mt-2 justify-content-center">
            <div class="col-md-10">
                <button type="button" data-url="{{route('admin-new-sessions.newAdvisor')}}"
                    id="enrole-consultant-session" class="btn btn-primary btn-block">
                    <i class="fas fa-plus"></i>
                </button>
            </div>
        </div>



        <div class="row mt-5 justify-content-center">
            <div class="col-md-10">
                <h4>Information about the reserved session</h4>
            </div>
        </div>
       


        <div class="row justify-content-center">
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group date" id="admin-session-add-book-start-date-time" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-toggle="datetimepicker" id="admin-session-add-book-start-date-time-input" data-target="#admin-session-add-book-start-date-time" />
                        <div class="input-group-append" data-target="#admin-session-add-book-start-date-time" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <div class="input-group date" id="admin-session-add-book-end-date-time" data-target-input="nearest">
                        <input type="text" class="form-control datetimepicker-input" data-toggle="datetimepicker" id="admin-session-add-book-end-date-time-input" data-target="#admin-session-add-book-end-date-time" />
                        <div class="input-group-append" data-target="#admin-session-add-book-end-date-time" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row mt-4 justify-content-center">
            <div class="col-md-10">
                <h4>Session price</h4>
            </div>
        </div>

        <div class="row mt-1 justify-content-center">
            <div class="col-md-10">
                <div class="input-group mb-3">
                    <input type="number" class="form-control" id="admin-session-add-price" required>
                    <div class="input-group-append">
                        <span class="input-group-text">€</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-4 justify-content-center">
            <div class="mt-2 col-md-10">
                <button type="submit" data-next="1" class="btn btn-success btn-block">Save</button>
            </div>
        </div>
    </form>
</div>

<div class="form-group">
    <div class="row">
        <div class="col-md-8">
            <div id="datetimepicker13"></div>
        </div>
    </div>
</div>


@endsection
