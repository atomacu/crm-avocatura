@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <h4>
        <i class="far fa-thumbs-up mr-2"></i>Ended sessions
    </h4>
    <hr>
    <div class="row text-white mx-1 bg-dark">
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Client name
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
           Advisors
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Seasson date and time start
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Seasson date and time end
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Price
        </div>
    </div>
    @foreach ($mails as $mail)
    @if(count($mail->session->consultants) != 0 && $mail->session->price!=null)
    <div data-redirect-url="{{route('admin-finished-sessions.show',['id'=>$mail->id,'lang'=>App::getLocale()])}}"
        class="row mx-1 bg-light bg-light-hover">

        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$mail->name}}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            @foreach ($mail->session->consultants as $consultant)
            <div class="row">
                <div class="col-md-12">
                    {{$consultant->user->name}}
                </div>
            </div>
            @endforeach
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$mail->session->date_time_start}}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$mail->session->date_time_end}}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$mail->session->price}} euro
        </div>
    </div>
    @endif
    @endforeach
</div>
@endsection
