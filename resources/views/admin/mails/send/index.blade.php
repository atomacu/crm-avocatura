@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <h4 class=""><i class="far fa-envelope mr-2"></i>E-mails</h4>
    <hr>
    <ul class="nav nav-tabs mb-2" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link" href="{{route("admin-mail.index",App::getLocale())}}">
                Received messages
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('group-email-template.index',App::getLocale())}}">
                Autoresponse templates
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="{{route('admin-contact-form.index',App::getLocale())}}">
                Send
            </a>
        </li>
    </ul>

    @foreach ($mails as $mail)
    <div data-redirect-url="{{route('admin-contact-form.show',['id'=>$mail->id,'lang'=>App::getLocale()])}}"
        class="row mx-1 bg-light bg-light-hover">
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md-1">
            {{$mail->id}}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$mail->name}}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$mail->email}}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$mail->phone}}
        </div>
    </div>
    @endforeach


</div>
@endsection
