@extends('layouts.admin')

@section('content')
<div class="container-fluid">

    <h4><i class="far fa-envelope mr-2"></i>E-mails</h4>
    <hr>
    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" href="{{route("admin-mail.index",App::getLocale())}}">Received messages</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('group-email-template.index',App::getLocale())}}">Autoresponse
                templates</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('admin-contact-form.index',App::getLocale())}}">Send</a>
        </li>
    </ul>

    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="email" role="tabpanel" aria-labelledby="email-tab">

            <div class="row mt-2 px-2">
                <div id="mail-table-col" class="col-md-12">
                    <div class="row px-2">
                        <div class="col-md-1 pl-0">
                            <div class="input-group mb-1">
                                <div class="input-group-prepend btn-block">
                                    <div class="input-group-text btn-block">
                                        <div class="custom-control pl-5 custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="email-check-all">
                                            <label class="custom-control-label" for="email-check-all"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1 px-0">
                            <button class="btn btn-dark btn-block" data-toggle="modal"
                                data-target="#delete-email-modal">
                                <i class="fas fa-trash-alt text-danger"></i>
                            </button>

                            <div class="modal fade" id="delete-email-modal" tabindex="-1" role="dialog"
                                aria-labelledby="delete-email-modal-label" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title text-center w-100" id="delete-email-modal-label">
                                                Are you sure that you want to delete this e-mails?
                                            </h5>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row justify-content-center">
                                                <div class="col-md-5">
                                                    <button type="button" class="btn btn-block btn-secondary"
                                                        data-dismiss="modal">Cancel</button>
                                                </div>
                                                <div class="col-md-5">
                                                    <button type="button" id="delete-emails-button"
                                                        class="btn btn-block btn-danger">Delete</button>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="m-0">
                    @foreach ($mails as $mail)
                    <div data-redirect-url="{{route('admin-contact-form.show',['id'=>$mail->id,'lang'=>App::getLocale()])}}"
                        class="row mx-1 bg-light bg-light-hover">

                        <div class="h7 p-2 text-center border-bottom col-md-0">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox"
                                    data-delete-url="{{route('admin-contact-form.destroy',$mail->id)}}"
                                    class="email-check custom-control-input" id="email-check-{{$mail->id}}">
                                <label class="custom-control-label" for="email-check-{{$mail->id}}"></label>
                            </div>
                        </div>
                        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md-1">
                            {{$mail->id}}
                        </div>
                        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
                            {{$mail->name}}
                        </div>
                        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
                            {{$mail->email}}
                        </div>
                        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
                            {{$mail->phone}}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-md-10">
                    {{ $mails->links() }}
                </div>
            </div>
        </div>


    </div>



</div>
@endsection
