@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <h4 class="mb-0">
        <i class="far fa-envelope mr-2"></i>{{$emailTemplateGroup->name}}
    </h4>
    <hr class="mt-1">
    @foreach ($emailTemplateGroup->templates as $template)
    <div class="row mt-4 justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-11">
                            <span class="text-capitalize mr-2 font-weight-bold">
                                {{$template->lang->name}}:
                            </span>
                            {{$template->title}}
                        </div>
                        <div class="col-md-1">
                            <i class="fas fa-edit text-success pointer h5" data-toggle="modal"
                                data-target="#edit-temlate-{{$template->id}}"></i>

                            <div class="modal fade" id="edit-temlate-{{$template->id}}" tabindex="-1" role="dialog"
                                aria-labelledby="edit-temlate-{{$template->id}}-label" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="edit-temlate-{{$template->id}}-label">
                                                <span class="text-capitalize mr-2 font-weight-bold">
                                                    {{$template->lang->name}}:
                                                </span>
                                            </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form data-url="{{route('email-template.update',$template->id)}}" data-lang-id="{{$template->lang_id}}" class="email-template-edit-form">
                                                <div class="row mb-2 justify-content-center">
                                                    <div class="col-md-10">
                                                        <input type="text" id="email-template-edit-{{$template->lang_id}}" value="{{$template->title}}" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="row mb-2 justify-content-center">
                                                    <div class="col-md-10">
                                                        <textarea class="editor" id="editor-{{$template->lang_id}}"
                                                            data-lang-id="{{$template->lang_id}}">{!!$template->description!!}</textarea>
                                                    </div>
                                                </div>
                                                <div class="row justify-content-center">
                                                    <div class="col-md-10">
                                                        <button class="btn btn-success btn-block">Save</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-body">
                    {!!$template->description!!}
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection
