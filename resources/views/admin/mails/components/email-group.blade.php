<div id="email-template-group-card-{{$emailTemplateGroup->id}}" class="col-md-3 mt-2">
    <div class="card  h-100">
        <div class="card-body">
            <a href="{{route('group-email-template.show',$emailTemplateGroup->id)}}">
                <h5 id="group-template-name-{{$emailTemplateGroup->id}}" class="card-title text-center">
                    {{$emailTemplateGroup->name}}
                </h5>
            </a>
            <div class="row my-2">
                <div class="col-md-12">
                    <div class="custom-control custom-switch">
                        <input type="checkbox"
                            data-url="{{ route('group-email-template.updateInUse',$emailTemplateGroup->id) }}"
                            value="{{$emailTemplateGroup->id}}" class="set-in-use-template custom-control-input"
                            id="set-in-use-template-{{$emailTemplateGroup->id}}" @if($emailTemplateGroup->in_use==1)
                        checked @endif>
                        <label class="custom-control-label" for="set-in-use-template-{{$emailTemplateGroup->id}}">
                            Set for automatic response
                        </label>
                    </div>
                </div>
            </div>
            <div class="row my-2">
                <div class="col-md-6">
                    <button class="btn btn-light btn-block" data-toggle="modal"
                        data-target="#edit-email-template-name-{{$emailTemplateGroup->id}}">
                        <i class="fas fa-edit text-success"></i>
                    </button>

                    <div class="modal fade" id="edit-email-template-name-{{$emailTemplateGroup->id}}" tabindex="-1"
                        role="dialog" aria-labelledby="edit-email-template-name-{{$emailTemplateGroup->id}}-label"
                        aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"
                                        id="edit-email-template-name-{{$emailTemplateGroup->id}}-label">
                                        Edit
                                        template group name</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form data-url="{{ route('group-email-template.update',$emailTemplateGroup->id)}}"
                                        data-id="{{$emailTemplateGroup->id}}" class="edit-template-group-name-form">
                                        <div class="row mb-2 justify-content-center">
                                            <div class="col-md-10">
                                                <input type="text"
                                                    id="edit-template-group-name-{{$emailTemplateGroup->id}}"
                                                    value="{{$emailTemplateGroup->name}}" class="form-control" required>
                                            </div>
                                        </div>

                                        <div class="row justify-content-center">
                                            <div class="col-md-10">
                                                <button class="btn btn-block btn-success">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <button class="btn btn-light btn-block">
                        <i class="fas fa-trash-alt text-danger" data-toggle="modal"
                            data-target="#delete-email-template-group-{{$emailTemplateGroup->id}}"></i>
                    </button>
                    <div class="modal fade" id="delete-email-template-group-{{$emailTemplateGroup->id}}" tabindex="-1"
                        role="dialog" aria-labelledby="delete-email-template-group-{{$emailTemplateGroup->id}}-label"
                        aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title text-center w-100"
                                        id="delete-email-template-group-{{$emailTemplateGroup->id}}-label">
                                        Are you sure that you
                                        want to delete this template group?</h5>
                                </div>
                                <div class="modal-body">
                                    <div class="row justify-content-center">
                                        <div class="col-md-5">
                                            <button type="button" class="btn btn-block btn-secondary"
                                                data-dismiss="modal">Cancel</button>
                                        </div>
                                        <div class="col-md-5">
                                            <button data-id="email-template-group-card-{{$emailTemplateGroup->id}}"
                                                data-url="{{ route('group-email-template.destroy',$emailTemplateGroup->id)}}"
                                                class="btn btn-danger delete-email-template-group btn-block">Delete</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
