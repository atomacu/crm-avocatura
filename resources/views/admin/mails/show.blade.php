@extends('layouts.admin')

@section('content')
<svg class="loader" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 340 340">
    <circle cx="170" cy="170" r="95" stroke="#404041" />
    <circle cx="170" cy="170" r="85" stroke="#E2007C" />
    <circle cx="170" cy="170" r="75" stroke="#404041" />
</svg>
<div class="container-fluid">
    <h4><i class="far fa-envelope mr-2"></i>E-mails</h4>
    <hr>
    <div class="jumbotron py-4">
        <h1>{{$email->name}}</h1>
        <p class="m-0">{{$email->email}}</p>
        <p class="m-0">{{$email->phone}}</p>
        <hr class="my-1">
        <p class="m-0">{{$email->description}}</p>
    </div>

    @if(!$email->response)
    <div class="row justify-content-center">
        <div class="col-md-12">
            <form data-url="{{route('admin-contact-form.store')}}" data-email-id="{{$email->id}}"
                id="response-contact-form-form">
                <div class="mb-3 row">
                    <div class="col-md-12">
                        <input type="text" placeholder="Subject" id="response-contact-form-subject"
                            class="form-control">
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-12">
                        <textarea class="editor" placeholder="E-Mail text" id="editor-1" data-lang-id="1"></textarea>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-success btn-block">Send</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @else
    <hr>
    <span class="font-weight-bold mr-2">Subject:</span>{{$email->response->title}}
    <hr>
    <span class="font-weight-bold mr-2">E-Mail:</span><br>
    {!!$email->response->description!!}
    <hr>
    @endif
</div>
@endsection
