<div style="width:100%;padding-right:10%;padding-left:10%;margin-right:auto;margin-left:auto;">
    {!!$text->description!!}
    <a href="{{route("sessions.create",['lang' => App::getLocale(),'id'=>$text->id])}}"
        style="text-decoration:none; color:white; text-align:center; margin-bottom:20px;padding:10px;background-color: #64a19d; display:block; width:80%;">
        Rezerva o sesiune
    </a>
</div>

<div
    style="padding:10px 20px;position:fixed;left:0;bottom:0;width:100%;background-color:#6c757d;color:white;text-align:center;">
    <p>Pentru a rezerva o sesiune accesati acest link:
        {{route("sessions.create",['lang' => App::getLocale(),'id'=>$text->id])}} </p>
</div>
