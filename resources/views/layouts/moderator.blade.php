<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">

</head>

<body class="sidebar-toggled">
    <div id="app">
        <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

            <a class="navbar-brand" href="{{ url('/') }}">
                RÖMISCHER
            </a>

            <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
                <i class="fas fa-bars"></i>
            </button>

            <!-- Navbar -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="text-capitalize nav-link" href="" id="navbarDropdown" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        {{App::getLocale()}}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach ($langs as $lang)
                        <a class="dropdown-item"
                            href="{{ route( Route::currentRouteName(),$lang->system_name ) }}">{{$lang->name}}</a>
                        @endforeach

                    </div>
                </li>
                <!-- Authentication Links -->
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif
                @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link text-center" href="#" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest
            </ul>

        </nav>
        <div id="wrapper">
            <ul class="sidebar navbar-nav toggled">
                <li
                    class="nav-item @if('admin-mail.index'==Route::currentRouteName() || 'group-email-template.index'==Route::currentRouteName() || 'admin-contact-form.index'==Route::currentRouteName()) active @endif">
                    <a class="nav-link active" href="{{route("admin-mail.index",App::getLocale())}}">
                        <i class="far fa-envelope"></i>
                        <span>E-Mails</span>
                    </a>
                </li>
                <li
                    class="nav-item @if('admin-new-sessions.index'==Route::currentRouteName() || 'admin-new-sessions.show'==Route::currentRouteName()) active @endif">
                    <a class="nav-link active" href="{{route("admin-new-sessions.index",App::getLocale())}}">
                        <i class="far fa-folder-open"></i>
                        <span>New sessions</span>
                    </a>
                </li>
                <li class="nav-item @if('admin-sessions.index'==Route::currentRouteName() || 'admin-sessions.show'==Route::currentRouteName()) active @endif">
                    <a href="{{ route('admin-sessions.index') }}" class="nav-link active">
                        <i class="fas fa-handshake"></i>
                        <span>Sessions</span>
                    </a>
                </li>
                <li class="nav-item @if('admin-finished-sessions.index'==Route::currentRouteName() || 'admin-finished-sessions.show'==Route::currentRouteName()) active @endif">
                    <a class="nav-link active" href="{{route("admin-finished-sessions.index",App::getLocale())}}">
                        <i class="far fa-thumbs-up"></i>
                        <span>Ended sessions</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="{{route("admin-new-sessions.index",App::getLocale())}}">
                        <i class="fab fa-fort-awesome"></i>
                        <span>Projects</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="{{route("admin-user-management.index",App::getLocale())}}">
                        <i class="far fa-money-bill-alt"></i>
                        <span>Client Management</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="{{route("admin-user-management.index",App::getLocale())}}">
                        <i class="far fa-bell"></i>
                        <span>Newsletter</span>
                    </a>
                </li>
            </ul>

            <div id="content-wrapper">

                @yield('content')

                <footer class="sticky-footer">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright © SoftChamp 2019</span>
                        </div>
                    </div>
                </footer>
            </div>

        </div>
    </div>
    <script src="{{ asset('js/jquery-3-4-1.js') }}" defer></script>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/sb-admin.js') }}" defer></script>
    <script src="{{ asset('js/script.js') }}" defer></script>
</body>

</html>
