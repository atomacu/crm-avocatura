<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <script src="https://cdn.ckeditor.com/ckeditor5/12.4.0/classic/ckeditor.js"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
        integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sb-admin.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/css/tempusdominus-bootstrap-4.min.css" />

</head>

<body class="sidebar-toggled">
    <div id="app">
        <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

            <a class="navbar-brand" href="{{ url('/') }}">
                RÖMISCHER
            </a>

            <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="#">
                <i class="fas fa-bars"></i>
            </button>

            <!-- Navbar -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="text-capitalize nav-link" href="" id="navbarDropdown" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        {{App::getLocale()}}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        @foreach ($langs as $lang)
                        <a class="dropdown-item"
                            href="{{ route( Route::currentRouteName(),$lang->system_name ) }}">{{$lang->name}}</a>
                        @endforeach

                    </div>
                </li>
                <!-- Authentication Links -->
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif
                @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link text-center" href="#" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest
            </ul>

        </nav>
        <div id="wrapper">
            <ul class="sidebar navbar-nav toggled">
                <li class="nav-item">
                    <a class="nav-link" href="{{route("home",App::getLocale())}}">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                <li class="nav-item @if('admin-mail.index'==Route::currentRouteName() || 'group-email-template.index'==Route::currentRouteName() || 'admin-contact-form.index'==Route::currentRouteName()) active @endif">
                    <a class="nav-link active" href="{{route("admin-mail.index",App::getLocale())}}">
                        <i class="far fa-envelope"></i>
                        <span>E-Mails</span>
                    </a>
                </li>
                <li class="nav-item dropdown
                @if('admin-new-sessions.index'==Route::currentRouteName() || 'admin-new-sessions.show'==Route::currentRouteName() || 'admin-finished-sessions.index'==Route::currentRouteName() || 'admin-finished-sessions.show'==Route::currentRouteName() || 'admin-sessions.index'==Route::currentRouteName() || 'admin-sessions.show'==Route::currentRouteName() )
                    active @endif">
                    <a class="nav-link dropdown-toggle" href="" id="pagesDropdown" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-handshake"></i>
                        <span>Sessions</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                        <a class="dropdown-item" href="{{route("admin-new-sessions.index",App::getLocale())}}">New
                            sessions</a>
                        <a class="dropdown-item"
                            href="{{ route('admin-sessions.index',App::getLocale()) }}">Sessions</a>
                        <a class="dropdown-item"
                            href="{{route("admin-finished-sessions.index",App::getLocale())}}">Ended sessions</a>
                    </div>
                </li>

                <li class="nav-item dropdown  @if('admin-new-projects.index'==Route::currentRouteName() || 'admin-projects.index'==Route::currentRouteName())
                    active  @endif ">
                    <a class="nav-link dropdown-toggle" href="" id="pagesDropdown" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-project-diagram"></i>
                        <span>Projects</span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                        <a class="dropdown-item" href="{{route("admin-new-projects.index",App::getLocale())}}">
                            New projects
                        </a>
                        <a class="dropdown-item" href="{{route("admin-projects.index",App::getLocale())}}">
                            Projects
                        </a>
                    </div>
                </li>
                <li class="nav-item @if('admin-calendar.index'==Route::currentRouteName()) active  @endif">
                    <a class="nav-link" href="{{route('admin-calendar.index',App::getLocale())}}">
                        <i class="far fa-calendar-alt"></i>
                        <span>Calendar</span>
                    </a>
                </li>
                <li class="nav-item @if('admin-tasks.index'==Route::currentRouteName()) active  @endif">
                    <a class="nav-link" href="{{route('admin-tasks.index',App::getLocale())}}">
                        <i class="fas fa-tasks"></i>
                        <span>Tasks</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">
                        <i class="fas fa-comment-alt"></i>
                        <span>Talks</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">
                        <i class="fas fa-cloud"></i>
                        <span>Cloud</span>
                    </a>
                </li>
                <li class="nav-item @if('admin-contact.index'==Route::currentRouteName()) active @endif">
                    <a class="nav-link" href="{{route('admin-contact.index',App::getLocale())}}">
                        <i class="fas fa-address-card"></i>
                        <span>Contacts</span>
                    </a>
                </li>
                
                <li class="nav-item @if('admin-user-management.index'==Route::currentRouteName()) active @endif">
                    <a class="nav-link active" href="{{route("admin-user-management.index",App::getLocale())}}">
                        <i class="fas fa-users"></i>
                        <span>User Management</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="{{route("admin-user-management.index",App::getLocale())}}">
                        <i class="far fa-money-bill-alt"></i>
                        <span>Client Management</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="{{route("admin-user-management.index",App::getLocale())}}">
                        <i class="far fa-bell"></i>
                        <span>Newsletter</span>
                    </a>
                </li>

                <li class="nav-item @if('admin-general-settings.index'==Route::currentRouteName()) active @endif">
                    <a class="nav-link active" href="{{route("admin-general-settings.index",App::getLocale())}}">
                        <i class="fas fa-cogs"></i>
                        <span>General settings</span>
                    </a>
                </li>
            </ul>

            <div id="content-wrapper">

                @yield('content')

                <footer class="sticky-footer">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright © SoftChamp 2019</span>
                        </div>
                    </div>
                </footer>
            </div>

        </div>
    </div>

    {{-- <script src="{{ asset('js/jquery-3-4-1.js') }}" defer></script> --}}
  
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/sb-admin.js') }}" defer></script>
    <script src="{{ asset('js/axios.js') }}" defer></script>
    <!-- Scripts -->
    
    
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tempusdominus-bootstrap-4/5.0.1/js/tempusdominus-bootstrap-4.min.js"></script>
    
    
    @if("admin-calendar.index" == Route::currentRouteName())
    
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
    <script src="http://code.jquery.com/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
    <script src="{{ asset('js/full-calendar.js') }}"></script>
    
    {!! $calendar_details->script() !!}
    @endif
    
    <script src="{{ asset('js/script.js') }}" defer></script>

</body>

</html>
