@extends('layouts.app')

@section('content')
<div class="container pt-5 vh-100">
    <div class="row mt-5 justify-content-center">
        <div class="col-md-9">
            <form @if(isset($id)) data-form-id="{{$id}}" @endif data-url="{{route('sessions.store')}}" id="booking-form">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center display-4">Book session</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <input id="book-session-date" type="date" class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 mt-4">
                        <input id="book-session-start-time" type="time" class="form-control">
                        <small class="text-center mt-3 form-text text-muted">Time when session will start.</small>
                    </div>
                    <div class="col-sm-6 mt-4">
                        <input id="book-session-end-time" type="time" class="form-control">
                        <small class="text-center mt-3 form-text text-muted">Time when session will end.</small>
                    </div>
                </div>
                <div class="row mt-4 ">
                    <div class="col-md-12">
                        <button class="btn btn-primary btn-block">Book</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<footer class="bg-black small text-center text-white-50">
    <div class="container">
        Copyright &copy; SoftChamp 2019
    </div>
</footer>

@endsection
