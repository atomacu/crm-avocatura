@extends('layouts.consultant')

@section('content')
<div class="container-fluid mb-5">
    <h4>
        <i class="fas fa-handshake mr-2"></i>Sessions
    </h4>
    <hr>

    <form data-url="{{ route('consultant-sessions.store') }}" id="admin-session-add-form">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h4>Client Info</h4>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-5">
                <label for="session-client-infor-name">Client name</label>
                <input id="admin-session-add-client-infor-name" class="form-control" type="text" required>
            </div>
            <div class="col-md-5">
                <label for="session-client-infor-email">Email address</label>
                <input id="admin-session-add-client-infor-email" class="form-control" type="text" required>
            </div>
        </div>
        <div class="row mt-2 justify-content-center">
            <div class="col-md-5">
                <label for="session-client-infor-phone">Phone number</label>
                <input id="admin-session-add-client-infor-phone" class="form-control" type="text" required>
            </div>
            <div class="col-md-5">
                <label for="session-client-infor-address">Address</label>
                <input id="admin-session-add-client-infor-address" placeholder="Address" class="form-control"
                    type="text">
            </div>
        </div>
        <div class="row mt-2 justify-content-center">
            <div class="col-md-5">
                <label for="session-client-infor-birth_date">Birth date</label>
                <input id="admin-session-add-client-infor-birth_date" placeholder="Birth date" class="form-control"
                    type="date">
            </div>
            <div class="col-md-5">
                <label for="session-client-infor-gender">Gender</label>
                <select name="" id="admin-session-add-client-infor-gender" class="form-control rounded-0"
                    id="my-select">
                    <option value="" selected disabled>Gender</option>
                    @foreach ($genders as $gender)
                    <option value="{{$gender->id}}">
                        {{$gender->genderTrans(App::getLocale())->name}}
                    </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row mt-2 justify-content-center">
            <div class="col-md-10">
                <textarea name="" placeholder="Description" class="form-control" id="editor-1" required></textarea>
            </div>
        </div>
        <div class="row mt-2 justify-content-center">
            <div class="col-md-10">
                <label for="exampleInputEmail1">Contact message</label>

            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-md-10">
                <h4>Advisors Enrollment</h4>
            </div>
        </div>

        <div class="row mt-2 justify-content-center">
            <div id="advisor-list" class="col-md-10">
                <div class="mb-2 row justify-content-center">
                    <div class="col-md-5">
                        <select name="" id="" class="advisor-engagement-select form-control">
                            @foreach ($users as $user)
                            @if(Auth::user()->id == $user->id)<option value="{{$user->id}}" selected>{{$user->name}}</option> @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group mb-3">
                            <input type="number" value="100" class="advisor-engagement-percentage form-control"
                                aria-label="advisor engagement percentage">
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row mt-2 justify-content-center">
            <div class="col-md-10">
                <button type="button" data-url="{{route('admin-new-sessions.newAdvisor')}}"
                    id="enrole-consultant-session" class="btn btn-primary btn-block">
                    <i class="fas fa-plus"></i>
                </button>
            </div>
        </div>



        <div class="row mt-5 justify-content-center">
            <div class="col-md-10">
                <h4>Information about the reserved session</h4>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-10">
                <input id="admin-session-add-book-date" type="date" class="form-control" required>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-sm-5 mt-4">
                <input id="admin-session-add-book-start-time" type="time" class="form-control" required>
                <small class="text-center mt-3 form-text text-muted">Time when session will start.</small>
            </div>
            <div class="col-sm-5 mt-4">
                <input id="admin-session-add-book-end-time" type="time" class="form-control" required>
                <small class="text-center mt-3 form-text text-muted">Time when session will end.</small>
            </div>
        </div>

        <div class="row mt-4 justify-content-center">
            <div class="col-md-10">
                <h4>Session price</h4>
            </div>
        </div>

        <div class="row mt-1 justify-content-center">
            <div class="col-md-10">
                <div class="input-group mb-3">
                    <input type="number" class="form-control" id="admin-session-add-price" required>
                    <div class="input-group-append">
                        <span class="input-group-text">€</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-4 justify-content-center">
            <div class="mt-2 col-md-10">
                <button type="submit" data-next="1" class="btn btn-success btn-block">Save</button>
            </div>
        </div>
    </form>
</div>
@endsection
