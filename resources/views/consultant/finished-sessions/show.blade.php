@extends('layouts.consultant')

@section('content')
<div class="container-fluid mb-5">
    <h4>
        <i class="far fa-thumbs-up mr-2"></i>Ended sessions
    </h4>
    <hr>

    <form data-url="{{ route('admin-sessions.update',$mail->id) }}" id="session-confirm-form">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h4>Client Info</h4>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-5">
                <label for="session-client-infor-name">Client name</label>
                <input value="{{$mail->name}}" id="session-client-infor-name" class="form-control" type="text" readonly>
            </div>
            <div class="col-md-5">
                <label for="session-client-infor-email">Email address</label>
                <input value="{{$mail->email}}" id="session-client-infor-email" class="form-control" type="text" readonly>
            </div>
        </div>
        <div class="row mt-2 justify-content-center">
            <div class="col-md-5">
                <label for="session-client-infor-phone">Phone number</label>
                <input value="{{$mail->phone}}" id="session-client-infor-phone" class="form-control" type="text" readonly>
            </div>
            <div class="col-md-5">
                <label for="session-client-infor-address">Address</label>
                <input value="{{$mail->address}}" id="session-client-infor-address" placeholder="Address"
                    class="form-control" type="text" readonly>
            </div>
        </div>
        <div class="row mt-2 justify-content-center">
            <div class="col-md-5">
                <label for="session-client-infor-birth_date">Birth date</label>
                <input value="{{$mail->birth_date}}" id="session-client-infor-birth_date" placeholder="Birth date"
                    class="form-control" type="date" readonly>
            </div>
            <div class="col-md-5">
                <label for="session-client-infor-gender">Gender</label>
                <select name="" id="session-client-infor-gender" class="form-control rounded-0" id="my-select">
                    <option value="" selected disabled>Gender</option>
                    @foreach ($genders as $gender)
                    <option value="{{$gender->id}}" @if($gender->id == $mail->gender_id) selected
                        @endif>{{$gender->genderTrans(App::getLocale())->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="row mt-2 justify-content-center">
            <div class="col-md-10">
                <label for="exampleInputEmail1">Contact message</label>
                <p>{{$mail->description}}</p>
            </div>
        </div>
        @if(isset($mail->response))
        <div class="row mt-3 justify-content-center">
            <div class="col-md-10">
                <h4 class="m-0">Manager's response</h4>
            </div>
        </div>

        <div class="row mt-1 mb-3 justify-content-center">
            <div class="col-md-10">
                <p class="my-1"><span class="font-weight-bold mr-2">Subject:</span>{{$mail->response->title}}</p>
                <p class="my-1"><span class="font-weight-bold mr-2">Email:</span></p>
                {!!$mail->response->description!!}
            </div>
        </div>
        @endif

        @if(isset($mail->session))
        <div class="row mt-5 justify-content-center">
            <div class="col-md-10">
                <h4>Information about the reserved session</h4>
            </div>
        </div>

        <div class="row mt-2 justify-content-center">
            <div class="col-md-10">
                <div class="row mx-1 bg-dark text-white">
                    <div class="pointer p-2 text-center border-bottom col-md">
                        Date
                    </div>
                    <div class="pointer p-2 text-center border-bottom col-md">
                        Time when the session starts
                    </div>
                    <div class="pointer p-2 text-center border-bottom col-md">
                        Time when the session ends
                    </div>
                </div>
                <div class="row mb-4 mx-1 bg-light bg-light-hover">
                    <div class="pointer p-2 text-center border-bottom col-md">
                        {{$mail->session->date}}
                    </div>
                    <div class="pointer p-2 text-center border-bottom col-md">
                        {{$mail->session->time_start}}
                    </div>
                    <div class="pointer p-2 text-center border-bottom col-md">
                        {{$mail->session->time_end}}
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="row justify-content-center">
            <div class="col-md-10">
                <h4>Advisors Enrollment</h4>
            </div>
        </div>

        <div class="row mt-2 justify-content-center">
            <div id="advisor-list" class="col-md-10">
                @foreach ($mail->session->consultants as $consultant)
                <div class="mb-2 row justify-content-center">
                    <div class="col-md-5">
                        <select name="" id="" class="advisor-engagement-select form-control">
                            @foreach ($users as $user)
                            @if($user->id == $consultant->user_id)<option value="{{$user->id}}" selected>{{$user->name}}</option>@endif
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <div class="input-group mb-3">
                            <input type="number" value="{{$consultant->engagement_rate}}"
                                class="advisor-engagement-percentage form-control"
                                aria-label="advisor engagement percentage" readonly>
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                    </div>
                </div>

                @endforeach
            </div>
        </div>


        <div class="row mt-4 justify-content-center">
            <div class="col-md-10">
                <h4>Session price</h4>
            </div>
        </div>

        <div class="row mt-1 justify-content-center">
            <div class="col-md-6">
                <div class="input-group mb-3">
                    <input type="number" class="form-control" id="session-price" value="{{$mail->session->price}}" readonly>
                    <div class="input-group-append">
                        <span class="input-group-text">€</span>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endsection
