@extends('layouts.consultant')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-10">
            <h4>
                <i class="fas fa-handshake mr-2"></i>Sessions
            </h4>
        </div>
        <div class="col-md-2">
            <a href="{{route('consultant-sessions.create',App::getLocale())}}" class="btn btn-primary btn-block"><i class="fas fa-plus mr-2"></i>Create new session</a>
        </div>
    </div>


    <hr class="mt-1">
    <div class="row text-white mx-1 bg-dark">
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Client name
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Advisors
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Seasson date
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Seasson time start
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Seasson time end
        </div>
        <div class="pointer a-redirect p-2 text-center border-bottom col-md">
            Price
        </div>
    </div>
    @foreach (Auth::user()->consultations as $consultation)
    @if($consultation->session->price!=null && $consultation->session->mail->status==2)
    <div data-redirect-url="{{route('consultant-sessions.show',['id'=>$consultation->session->mail->id,'lang'=>App::getLocale()])}}"
        class="row mx-1 bg-light bg-light-hover">

        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$consultation->session->mail->name}}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{Auth::user()->name}}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$consultation->session->date}}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$consultation->session->time_start}}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$consultation->session->time_end}}
        </div>
        <div class="pointer h7 a-redirect p-2 text-center border-bottom col-md">
            {{$consultation->session->price}} euro
        </div>
    </div>
    @endif
    @endforeach
</div>
@endsection
